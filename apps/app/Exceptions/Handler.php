<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\Exception\FatalErrorException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use PHPMailer\PHPMailer;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
            if($e instanceof NotFoundHttpException)
        {
            return response()->view('errors/404', [], 404);
        }
        elseif (app()->environment() == 'production') // running when " production "
        {
            if ($e instanceof ValidationException) 
            {
                return parent::render($request, $e); 
            } 
            elseif($e instanceof \ErrorException)
            {
                # save information log into tbl error_log
                $this->error_information_save($e);
                return response()->view('errors.500', [], 500); 
            }
            elseif($e instanceof \Symfony\Component\Debug\Exception\FatalErrorException)
            {
                # save information log into tbl error_log
                $this->error_information_save($e);
                return response()->view('errors.500', [], 500); 
            }
            else
            {
                return response()->view('errors.500', [], 500); 
            }
        }
        return parent::render($request, $e);
    }

    function error_information_save($e)
    {
        $db = 'Illuminate\Support\Facades\DB';
        $now = date('Y-m-d H:i:s');
        
        $error_info = array(
            # error message
            'error_log_message' => $e->getMessage(), // get message
            'error_log_file' => $e->getFile(), // this is get file
            'error_log_line' => $e->getLine(), // this is get line
            'error_log_url' => $_SERVER['REQUEST_URI'], // this is url
            # error message

            # date log error 
            'error_log_date' => date('Y-m-d H:i:s', strtotime($now. "+7 hours")),
        );
        // 
        #send email error-notif@barantum.com
        $now = date('Y-m-d H:i:s');

        // $to = "error-notif@barantum.com";
        // $subject = 'Error Log at: '.date('Y-m-d H:i:s', strtotime($now. "+7 hours"));
        // $txt = implode(" | ",$error_info);
        // $headers = "From: ".Config('setting.EMAIL_FROM');

        // mail($to,$subject,$txt,$headers);
        $mail = new PHPMailer\PHPMailer(true); 
        try {
          $mail->isSMTP(true);
          $mail->CharSet = "utf-8";
          $mail->SMTPAuth = true;
          $mail->SMTPSecure = "ssl";
          $mail->Port       = 465;
          $mail->Host       = "smtp.gmail.com";
          $mail->Username = "mail.error.codenub@gmail.com";
          $mail->Password = "rendi123";
          $mail->setFrom("email.error.codenub@gmail.com", "SMK ERROR NOTIF");
          $mail->Subject = 'Error Log at: '.date('Y-m-d H:i:s', strtotime($now. "+7 hours"));
          $mail->addAddress( "rendifebrian441@gmail.com");
          $mail->IsHTML(true);
          $mail->Body = implode(" | ",$error_info);
          $mail->send();
        
        } 
        catch (phpmailerException $e) 
        {
          $respon = "Please check your connection!";
        } 
        catch (Exception $e) 
        {
            $respon     = "Please check your connection!";
        }

        $respon     = "success";

        return true;
    }
}
