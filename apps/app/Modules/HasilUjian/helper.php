<?php
namespace App\Modules\HasilUjian;

class HasilUjianHelper
{
  var $model_hasilujian        = 'App\Modules\HasilUjian\Models\HasilUjian';
  var $model_kelas        = 'App\Modules\Kelas\Models\Kelas';
  var $model_jurusan        = 'App\Modules\Jurusan\Models\Jurusan';
  var $model_siswa        = 'App\Modules\Siswa\Models\Siswa';
  var $model_soal        = 'App\Modules\Soal\Models\Soal';

  function daftar_jurusan()
  {
    $result = array();

    $query = $this->model_jurusan::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function daftar_kelas()
  {
    $result = array();

    $query = $this->model_kelas::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function find_hasil_by_criteria($criteria)
  {
    $result = array();
    /* cari siswa dulu terus masing" siswa cari hasil ujian nya*/
    $find = $this->model_siswa::where("siswa.kelas_id_siswa",$criteria['kelas_id'])
                              ->where("siswa.jurusan_id_siswa",$criteria['jurusan_id']);
                              $find->rightjoin('hasil_ujian', function($join) use ($criteria)
                              { 
                                  $join->on('hasil_ujian.siswa_id_hasil_ujian', '=', 'siswa.serial_id_siswa')
                                  ->where('hasil_ujian.kelas_id_hasil_ujian', '=', $criteria['kelas_id'])
                                  ->where('hasil_ujian.jurusan_id_hasil_ujian', '=', $criteria['jurusan_id'])
                                  ->where('hasil_ujian.mata_pelajaran_id_hasil_ujian', '=', $criteria['mata_pelajaran_id']);
                              });

                              $find = $find->get();

    if (count($find)) 
    {
      $result = json_decode(json_encode($find), true);
      
      foreach ($result as $key => $value) 
      {
        $result[$key]['jawaban_hasil_ujian'] = json_decode($value['jawaban_hasil_ujian']); 
        $result[$key]['final_hasil_ujian'] = json_decode($value['final_hasil_ujian']); 
      }
    }

    return $result;
  }

  function detail_hasil_ujian($id)
  {
    $result = array();
    /* cari siswa dulu terus masing" siswa cari hasil ujian nya*/
    $find = $this->model_hasilujian::where("hasil_ujian.serial_id_hasil_ujian", $id)
                                  ->leftjoin("kelas as k", "k.serial_id_kelas", "=", "hasil_ujian.kelas_id_hasil_ujian")
                                  ->leftjoin("jurusan as j", "j.serial_id_jurusan", "=", "hasil_ujian.jurusan_id_hasil_ujian")
                                  ->leftjoin("siswa as s", "s.serial_id_siswa", "=", "hasil_ujian.siswa_id_hasil_ujian")
                                  ->leftjoin("mata_pelajaran as mp", "mp.serial_id_mata_pelajaran", "=", "hasil_ujian.mata_pelajaran_id_hasil_ujian")
                                  ->first();

    if (count($find)) 
    {
      $result = json_decode(json_encode($find), true);

      $result['jawaban_hasil_ujian'] = json_decode($result['jawaban_hasil_ujian'], true); 
      array_multisort(array_column($result['jawaban_hasil_ujian'], 'nomor_soal'), SORT_ASC, $result['jawaban_hasil_ujian']);

      $result['final_hasil_ujian'] = json_decode($result['final_hasil_ujian'], true); 

      $result['total_soal'] = $this->model_soal::select("kunci_jawaban_soal", "nomor_soal")->where("mata_pelajaran_id_soal", $result['mata_pelajaran_id_hasil_ujian'])
                              ->where("kelas_id_soal", $result['kelas_id_hasil_ujian'])
                              ->where("jurusan_id_soal", $result['jurusan_id_hasil_ujian'])
                              ->orderBy("nomor_soal", "ASC")
                              ->count();
    }

    return $result;
  }

  

}