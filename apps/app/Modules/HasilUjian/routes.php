<?php

Route::group(['module' => 'HasilUjian', 'middleware' => ['web'], 'namespace' => 'App\Modules\HasilUjian\Controllers'], function() {

    Route::get('hasil-ujian', 'HasilUjianController@index');
    Route::get('cari-hasil-by-criteria', 'HasilUjianController@find_hasil_by_criteria');
    Route::get('hasil-ujian/detail/{id}', 'HasilUjianController@detail_hasil_ujian');

});
