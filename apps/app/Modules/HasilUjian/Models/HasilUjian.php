<?php

namespace App\Modules\HasilUjian\Models;

use Illuminate\Database\Eloquent\Model;

class HasilUjian extends Model {

    protected $table 		= "hasil_ujian";
	protected $primaryKey 	= "serial_id_hasil_ujian";
	protected $guarded = array('serial_id_hasil_ujian');
	public $timestamps = false;

}
