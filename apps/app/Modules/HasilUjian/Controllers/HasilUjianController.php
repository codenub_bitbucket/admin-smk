<?php

namespace App\Modules\HasilUjian\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\HasilUjian\HasilUjianHelper as module_helper;

class HasilUjianController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $helper = new module_helper();
        $daftar_kelas = $helper->daftar_kelas();
        $daftar_jurusan = $helper->daftar_jurusan();
        $view = 'HasilUjian::index';

		$view_data = array(
            'daftar_kelas'               => $daftar_kelas,
			'daftar_jurusan'              => $daftar_jurusan,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    public function find_hasil_by_criteria(Request $request)
    {
        $helper = new module_helper();
        $data = $helper->find_hasil_by_criteria($request->all());

		return $data;
    }

    public function detail_hasil_ujian($id)
    {
        $id = base64_decode($id);
        $helper = new module_helper();
        $detail_data = $helper->detail_hasil_ujian($id);
        $view = 'HasilUjian::detail';

        if (count($detail_data) == 0) 
        {
            return redirect('hasil-ujian');
        }

		$view_data = array(
            'detail'               => $detail_data,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }
}
