@extends('layouts.app')

@section('title', 'Hasil Ujian')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item active"><a href="#">Hasil Ujian</a></li>
@endsection

@section('content')
<div class="">
  <div class="card">
    <div class="card-header">
      <h2 class="card-title">Hasil Ujian</h2>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div>
        <div class="form-group row">
          <label for="kelas_id" class="col-sm-2 col-form-label">Kelas</label>
          <div class="col-sm-10">
            <select class="form-control kelas_id" name="kelas_id">
              <option value="">- Pilih Kelas -</option>
              @foreach($daftar_kelas as $key => $row)
              <option value="{{ $row['serial_id_kelas'] }}">{{ $row['nomor_kelas']}} ({{ $row['nama_kelas']}})</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="jurusan_id" class="col-sm-2 col-form-label">Jurusan</label>
          <div class="col-sm-10">
            <select class="form-control jurusan_id" name="jurusan_id" disabled>
              <option value="">- Pilih Jurusan -</option>
              @foreach($daftar_jurusan as $key => $row)
              <option value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan']}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="mata_pelajaran_id" class="col-sm-2 col-form-label">Mata Pelajaran</label>
          <div class="col-sm-10">
            <select class="form-control mata_pelajaran_id" name="mata_pelajaran_id" disabled="true">
            </select>
          </div>
        </div>
      </div>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nama</th>
          <th>NISN</th>
          <th>Jawaban Benar</th>
          <th>Jawaban Salah</th>
          <th>Tidak Dijawab</th>
          <th>Nilai Akhir</th>
          <th>###</th>
        </tr>
        </thead>
        <tbody class="hasil_ujian">
          
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" ></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" ></script>

<script>
  $('.kelas_id').on('change', function() {
    $('.jurusan_id').prop("disabled", false);
  });

  $('.jurusan_id').on('change', function() {
    var kelas = $('.kelas_id').val();
    var jurusan = $('.jurusan_id').val();
    $.ajax({
        url: "{{ URL('mata-pelajaran/cari') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
        dataType: 'JSON',
        success: function (data) { 
            
          if (data.length > 0) 
          {
            $(".mata_pelajaran_id").prop("disabled", false);
            $(".mata_pelajaran_id").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
            $.each(data,function(key, value)
            {
                $(".mata_pelajaran_id").append('<option value=' + value.serial_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
            });  
          }
          else
          {
            $(".mata_pelajaran_id").prop("disabled", true);
            $(".mata_pelajaran_id").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
          }
            
        }
    }); 
  });

  $('.kelas_id').on('change', function() {
    if ($('.jurusan_id').val() !== "" && $('.jurusan_id').val() !== undefined) 
    {
      var kelas = $('.kelas_id').val();
      var jurusan = $('.jurusan_id').val();
      $.ajax({
          url: "{{ URL('mata-pelajaran/cari') }}",
          type: 'GET',
          data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
          dataType: 'JSON',
          success: function (data) { 
              
            if (data.length > 0) 
            {
              $(".mata_pelajaran_id").prop("disabled", false);
              $(".mata_pelajaran_id").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
              data.forEach(item => {
                  $(".mata_pelajaran_id").append('<option value=' + item.serial_id_mata_pelajaran + '>' + item.nama_mata_pelajaran + '</option>');
              });
            }
            else
            {
              $(".mata_pelajaran_id").prop("disabled", true);
              $(".mata_pelajaran_id").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
            }
              
          }
      });   
    }
  });

  $('.mata_pelajaran_id').on('change', function() {
    var kelas = $('.kelas_id').val();
    var jurusan = $('.jurusan_id').val();
    var mapel = $('.mata_pelajaran_id').val();
    $.ajax({
        url: "{{ URL('cari-hasil-by-criteria') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}&mata_pelajaran_id=${mapel}`,
        dataType: 'JSON',
        success: function (data) { 
            
          if (data.length > 0) 
          {
            var inner = "";
            data.forEach(item => {
              inner += `<tr>
                            <td>${item.nama_siswa}</td>
                            <td>${item.nisn_siswa}</td>
                            <td>${item.final_hasil_ujian.jawaban_benar}</td>
                            <td>${item.final_hasil_ujian.jawaban_salah}</td>
                            <td>${item.final_hasil_ujian.jawaban_kosong}</td>
                            <td>${item.final_hasil_ujian.nilai}</td>
                            <td>
                              <a class="btn btn-primary btn-sm" href="{{ URL('hasil-ujian/detail/${btoa(item.serial_id_hasil_ujian)}') }}">Detail</a>
                            </td>
                          </tr>
                        `;
            });
            $("#example1").dataTable().fnClearTable(); //clear the table
            $('#example1').dataTable().fnDestroy(); //destroy the datatable
            $('.hasil_ujian').html(inner);  //add your new data
            $('#example1').DataTable({  //redraw with new data
              "responsive": true,
              "autoWidth": false,
              "dom": 'Bfrtip',
              "buttons": [
                  'excel', 'pdf', 'print'
              ],
              "paging": true,
            });

            $('.dt-button').addClass("btn btn-primary btn-sm")
          }
          else
          {
            $("#example1").dataTable().fnClearTable(); //clear the table
            $('#example1').dataTable().fnDestroy(); //destroy the datatable
            $('#example1').DataTable({  //redraw with new data
              "responsive": true,
              "autoWidth": false,
            });
          }
            
        }
    });  
  });
</script>
@endsection