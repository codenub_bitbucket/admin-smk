@extends('layouts.app')

@section('title', 'Detail Hasil Ujian')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item"><a href="{{ URL('hasil-ujian') }}">Hasil Ujian</a></li>
<li class="breadcrumb-item active"><a href="#">Detail Hasil Ujian</a></li>
@endsection

@section('content')
<div class="">
  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                  src="{{ URL('assets/image/profile.png') }}">
          </div>

          <h3 class="profile-username text-center">{{ $detail['nama_siswa'] }}</h3>

          <p class="text-muted text-center">{{ $detail['nomor_kelas'] }} {{ $detail['nama_jurusan'] }} {{ strtoupper($detail['sub_jurusan_siswa']) }}</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Jawaban Benar</b> <a class="float-right">{{ $detail['final_hasil_ujian']['jawaban_benar'] }}</a>
            </li>
            <li class="list-group-item">
              <b>Jawaban Salah</b> <a class="float-right">{{ $detail['final_hasil_ujian']['jawaban_salah'] }}</a>
            </li>
            <li class="list-group-item">
              <b>Tidak Dijawab</b> <a class="float-right">{{ $detail['final_hasil_ujian']['jawaban_kosong'] }}</a>
            </li>
          </ul>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="card card-primary">
        <div class="card-header">
          <h2>Hasil Ujian Siswa</h2>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="">
            <div class="card-body">
              <blockquote>
                <table>
                  <tbody>
                    <tr>
                      <td>
                        Mata Pelajaran
                      </td>
                      <td>
                        :
                      </td>
                      <td>
                      &nbsp; {{ $detail['nama_mata_pelajaran'] }}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Jumlah Soal
                      </td>
                      <td>
                        :
                      </td>
                      <td>
                        &nbsp; {{ $detail['total_soal'] }} Soal
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Waktu Pengerjaan
                      </td>
                      <td>
                        :
                      </td>
                      <td>
                        &nbsp; {{ $detail['durasi_pengerjaan_mata_pelajaran'] }} Menit
                      </td>
                    </tr>
                  </tbody>
                </table>
              </blockquote>
            </div>
            <table style="width: 90%;" class="m-auto">
              <tbody>
                <tr>
                  <td style="vertical-align: text-top;width: 20%;">
                    <ul class="list-unstyled">
                      @for ($i = 0; $i < 10; $i++)
                      <li>{{ isset($detail['jawaban_hasil_ujian'][$i]) ? $detail['jawaban_hasil_ujian'][$i]['nomor_soal'] .'. '. $detail['jawaban_hasil_ujian'][$i]['jawaban'] : '' }}</li>
                      @endfor
                    </ul>
                  </td>
                  <td style="vertical-align: text-top;width: 20%;">
                    <ul class="list-unstyled">
                      @for ($i2 = 10; $i2 < 20; $i2++)
                      <li>{{ isset($detail['jawaban_hasil_ujian'][$i2]) ? $detail['jawaban_hasil_ujian'][$i2]['nomor_soal'] .'. '. $detail['jawaban_hasil_ujian'][$i2]['jawaban'] : '' }}</li>
                      @endfor
                    </ul>
                  </td>
                  <td style="vertical-align: text-top;width: 20%;">
                    <ul class="list-unstyled">
                      @for ($i3 = 20; $i3 < 30; $i3++)
                      <li>{{ isset($detail['jawaban_hasil_ujian'][$i3]) ? $detail['jawaban_hasil_ujian'][$i3]['nomor_soal'] .'. '. $detail['jawaban_hasil_ujian'][$i3]['jawaban'] : '' }}</li>
                      @endfor
                    </ul>
                  </td>
                  <td style="vertical-align: text-top;width: 20%;">
                    <ul class="list-unstyled">
                      @for ($i4 = 30; $i4 < 40; $i4++)
                      <li>{{ isset($detail['jawaban_hasil_ujian'][$i4]) ? $detail['jawaban_hasil_ujian'][$i4]['nomor_soal'] .'. '. $detail['jawaban_hasil_ujian'][$i4]['jawaban'] : '' }}</li>
                      @endfor
                    </ul>
                  </td>
                  <td style="vertical-align: text-top;width: 20%;">
                    <ul class="list-unstyled">
                      @for ($i5 = 40; $i5 < 50; $i5++)
                      <li>{{ isset($detail['jawaban_hasil_ujian'][$i5]) ? $detail['jawaban_hasil_ujian'][$i5]['nomor_soal'] .'. '. $detail['jawaban_hasil_ujian'][$i5]['jawaban'] : '' }}</li>
                      @endfor
                    </ul>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
@endsection