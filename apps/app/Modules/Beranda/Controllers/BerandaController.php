<?php

namespace App\Modules\Beranda\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\MataPelajaran\MataPelajaranHelper as mapel_helper;
use App\Modules\Siswa\SiswaHelper as siswa_helper;


class BerandaController extends Controller
{
    /**
     * Class constructor.
     */
    function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $m_helper = new mapel_helper();
        $s_helper = new siswa_helper();
        $daftar_mapel = $m_helper->daftar_data();
        $daftar_siswa = $s_helper->daftar_data();
        $view = 'Beranda::index';

		$view_data = array(
			'count_mapel'      => count($daftar_mapel),
			'count_siswa'              => count($daftar_siswa),
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
