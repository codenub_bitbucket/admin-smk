<?php

Route::group(['module' => 'Beranda', 'middleware' => ['web'], 'namespace' => 'App\Modules\Beranda\Controllers'], function() {

    Route::get('beranda', 'BerandaController@index');

});
