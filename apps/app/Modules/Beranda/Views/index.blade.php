@extends('layouts.app')

@section('title', 'Beranda')
@section('breadcrumb')
<li class="breadcrumb-item active"><a href="#">Beranda</a></li>
@endsection

@section('content')
<div class="">
  <!-- info top -->
  <div class="row">
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box bg-gradient-info">
        <span class="info-box-icon"><i class="fas fa-user-graduate"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Siswa</span>
          <span class="info-box-number">{{ $count_siswa }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box bg-gradient-success">
        <span class="info-box-icon"><i class="fas fa-book"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Pelajaran</span>
          <span class="info-box-number">{{ $count_mapel }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  <!-- Quick action -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box bg-gradient-success" style="cursor:pointer" onclick="list_kelas()">
        <span class="info-box-icon"><i class="fas fa-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Tambah Pelajaran</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box bg-gradient-info" style="cursor:pointer" onclick="tambah_siswa()">
        <span class="info-box-icon"><i class="fas fa-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Tambah Siswa</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box bg-gradient-warning" style="cursor:pointer" onclick="tambah_soal()">
        <span class="info-box-icon"><i class="fas fa-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Tambah Soal</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box bg-gradient-danger" style="cursor:pointer" onclick="hasil_ujian()">
        <span class="info-box-icon"><i class="fas fa-clipboard-list"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Hasil Ujian</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  <hr class="style-seven">

  <div class="row expand-action-kelas" style="display: none;">

  </div>
</div>

<style>
  hr.style-seven {
    overflow: visible; /* For IE */
    height: 30px;
    border-style: solid;
    border-color: black;
    border-width: 1px 0 0 0;
    border-radius: 20px;
  }
  hr.style-seven:before {
      display: block;
      content: "";
      height: 30px;
      margin-top: -31px;
      border-style: solid;
      border-color: black;
      border-width: 0 0 1px 0;
      border-radius: 20px;
  }
</style>

<script>
function list_kelas()
{
  $.ajax({
      url: "{{ URL('kelas/list_kelas') }}",
      type: 'GET',
      data: {},
      dataType: 'JSON',
      success: function (data) { 
        if (data.length > 0) 
        {
          $('.expand-action-kelas').css("display", "");
          $(".expand-action-kelas").html("");

          data.forEach(item => {  
            $('.expand-action-kelas').append(`<div class="col-md-4 col-sm-6 col-12">
              <div class="info-box bg-gradient-success" style="cursor:pointer" onclick="tambah_pelajaran('${item.kode_kelas}')">
                <span class="info-box-icon"><i class="fas fa-user"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Kelas ${item.nomor_kelas} (${item.nama_kelas})</span>
                </div>
              </div>
            </div>`);
          });
        }
      }
  }); 
}

function tambah_pelajaran(kelas)
{
  window.location = `{{ URL('mata-pelajaran/tambah/${kelas}') }}`  
}

function tambah_siswa()
{
  $(".expand-action-kelas").html("");
  window.location = `{{ URL('siswa/tambah') }}`  
}

function tambah_soal()
{
  $(".expand-action-kelas").html("");
  window.location = `{{ URL('soal/tambah') }}`  
}

function hasil_ujian()
{
  $(".expand-action-kelas").html("");
  window.location = `{{ URL('hasil-ujian') }}`  
}
</script>

@endsection