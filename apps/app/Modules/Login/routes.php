<?php

Route::group(['module' => 'Login', 'middleware' => ['web'], 'namespace' => 'App\Modules\Login\Controllers'], function() {

    Route::get('masuk', 'LoginController@index');
    
    Route::post('masuk', 'LoginController@masuk');
    
    Route::post('daftar-user', 'LoginController@daftar_user');

    Route::get('set_session', 'LoginController@set_session');

});
