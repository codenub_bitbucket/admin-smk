<?php

namespace App\Modules\Login\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Login\LoginHelper as modul_helper;
use App\User;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{

    public function index()
    {

        if (Auth::user()) 
        {
            return Redirect::to('beranda');
        }
        else 
        {
            return view("Login::index");
        }
    }

    public function masuk(Request $request)
    {
        $akun = $request->only(['email', 'password']);
        
        
        if (!Auth::attempt($akun)) 
        {
            return redirect()->back()->with('message', "Akun yg anda masukan salah!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }

        $request->session()->put('users_id', Auth::user()->id);
		$request->session()->put('users_name', Auth::user()->name);
		$request->session()->put('users_email', Auth::user()->email);
		$request->session()->put('secret', base64_encode($request->password));

        return Redirect::to('beranda');
    }

    public function daftar_user(Request $request)
    {
        $result = "false";
        $post_data = $request->all(); 
        if ($post_data['password_api'] == 'Rendi123!') 
        {
            $insert = array(
                'name'  => (isset($request['name']) && $request['name'] !== "") ? $request['name'] : "No Name",
                'email' => $request['email'],
                'password' => bcrypt($request['password'])
            );
            $query = User::create($insert);

            if ($query) 
            {

                $result = "true";
            }
        }

        return $result;
    }

    public function set_session(Request $request)
    {
        // echo "<pre>"; print_r("kontol"); exit;

        $request->session()->put('users_id', Auth::user()->id);
		$request->session()->put('users_name', Auth::user()->name);
		$request->session()->put('users_email', Auth::user()->email);

        return Redirect::to('beranda');
    }

}
