<?php

Route::group(['module' => 'Jurusan', 'middleware' => ['api'], 'namespace' => 'App\Modules\Jurusan\Controllers'], function() {

    Route::resource('Jurusan', 'JurusanController');

});
