<?php

Route::group(['module' => 'Jurusan', 'middleware' => ['web'], 'namespace' => 'App\Modules\Jurusan\Controllers'], function() {

    Route::resource('Jurusan', 'JurusanController');

});
