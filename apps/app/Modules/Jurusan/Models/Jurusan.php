<?php

namespace App\Modules\Jurusan\Models;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model {

    protected $table 		= "jurusan";
	protected $primaryKey 	= "serial_id_jurusan";
	protected $guarded = array('serial_id_jurusan');
	public $timestamps = false;

}
