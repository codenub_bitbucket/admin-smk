<?php
namespace App\Modules\Kelas;

class KelasHelper
{
  var $model_Kelas        = 'App\Modules\Kelas\Models\Kelas';

  function list_kelas()
  {
    $result = array();

    $query = $this->model_Kelas::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }
}