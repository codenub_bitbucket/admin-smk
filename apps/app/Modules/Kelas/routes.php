<?php

Route::group(['module' => 'Kelas', 'middleware' => ['web'], 'namespace' => 'App\Modules\Kelas\Controllers'], function() {

    Route::get('kelas/list_kelas', 'KelasController@get_list_kelas');

});
