<?php

namespace App\Modules\Kelas\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Kelas\KelasHelper as module_helper;

class KelasController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view("Kelas::index");
    }

    public function get_list_kelas(Request $request)
    {
        $helper = new module_helper();
        $list_kelas = $helper->list_kelas($request->all());

		return $list_kelas;
    }
}
