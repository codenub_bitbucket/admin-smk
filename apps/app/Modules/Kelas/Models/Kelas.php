<?php

namespace App\Modules\Kelas\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model {

    protected $table 		= "kelas";
	protected $primaryKey 	= "serial_id_kelas";
	protected $guarded = array('serial_id_kelas');
	public $timestamps = false;

}
