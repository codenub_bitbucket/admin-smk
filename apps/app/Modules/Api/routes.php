<?php

Route::group(['module' => 'Api', 'middleware' => ['web'], 'prefix' => 'api/v1', 'namespace' => 'App\Modules\Api\Controllers'], function() {

    /* Authentication */
    Route::post('siswa/login','ApiController@siswa_login');

    /* Mapel */
    Route::post('mata-pelajaran/cari','ApiController@find_mata_pelajaran');

    /* Soal */
    Route::post('soal/soal-by-mata-pelajaran','ApiController@soal_by_mata_pelajaran');
    Route::post('soal/simpan_hasil','ApiController@simpan_hasil_pekerjaan');

});
