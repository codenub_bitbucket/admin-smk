<?php
namespace App\Modules\Api;
use Illuminate\Support\Facades\DB;

class ApiHelper
{
  var $model_siswa                = 'App\Modules\Siswa\Models\Siswa';
	var $model_matapelajaran        = 'App\Modules\MataPelajaran\Models\MataPelajaran';
	var $model_soal                 = 'App\Modules\Soal\Models\Soal';
	var $model_hasil_ujian          = 'App\Modules\HasilUjian\Models\HasilUjian';

  function siswa_login($criteria)
  {
    $result = array();
    $result['status'] = 500;


    $query = $this->model_siswa::where("username_siswa", $criteria["username"])
                                ->leftjoin("kelas", "kelas.serial_id_kelas", "=", "siswa.kelas_id_siswa")
                                ->leftjoin("jurusan", "jurusan.serial_id_jurusan", "=", "siswa.jurusan_id_siswa")
                                ->first();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);

      if ($criteria['password'] == base64_decode($query['password_siswa'])) 
      {
        $result['status'] = 200;
        $result['data']   = $query;
      }
      else
      {
        $result['status'] = 401;
      }
    }

    return $result;
  }

  function find_mata_pelajaran($criteria)
  {
    $result = array();

    $query = $this->model_matapelajaran::where('kelas_id_mata_pelajaran', $criteria['kelas_id'])
                                       ->where('jurusan_id_mata_pelajaran', $criteria['jurusan_id'])
                                        ->whereBetween('tanggal_ujian_mata_pelajaran', [date('Y-m-d', strtotime("-7 day")), date("Y-m-d")])
                                       ->get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
      foreach ($result as $key => $value) 
      {
        $result[$key]['total_soal'] = $this->model_soal::where('mata_pelajaran_id_soal', '=', $value['serial_id_mata_pelajaran'])
                                                              ->count();

        $check_status = $this->model_hasil_ujian::where('siswa_id_hasil_ujian', $criteria['siswa_id'])
                                                ->where('kelas_id_hasil_ujian', $criteria['kelas_id'])
                                                ->where('jurusan_id_hasil_ujian', $criteria['jurusan_id'])
                                                ->where('mata_pelajaran_id_hasil_ujian', $value['serial_id_mata_pelajaran'])
                                                ->first();

        if (count($check_status)) 
        {
          $result[$key]['status_soal'] = 'done';
        }
        else
        {
          $result[$key]['status_soal'] = 'not_done';
        }
      }
    }

    return $result;
  }

  function soal_by_mata_pelajaran($id)
  {
    $result = array();
    $query = $this->model_matapelajaran::select(DB::raw("s.nomor_soal, s.deskripsi_soal, s.jawaban_soal"))
                                ->where("mata_pelajaran.serial_id_mata_pelajaran", $id)
                                ->rightjoin('soal as s', "s.mata_pelajaran_id_soal", "=", "mata_pelajaran.serial_id_mata_pelajaran")
                                ->leftjoin("kelas as k", "k.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->leftjoin("jurusan as j", "j.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->orderBy("s.nomor_soal", "ASC")
                                ->get();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);

      foreach ($query as $key => $value) 
      {
        $query[$key]['jawaban_soal'] = json_decode($value['jawaban_soal']);
      }

      $result = $query;
    }

    return $result;
  }

  function simpan_hasil_pekerjaan($criteria)
  {
    $result = false;

    if (count($criteria)) 
    {
      $criteria_soal = array(
        'kelas_id'            => (isset($criteria['kelas_id']) && $criteria['kelas_id'] > 0) ? $criteria['kelas_id'] : 0,
        'jurusan_id'          => (isset($criteria['jurusan_id']) && $criteria['jurusan_id'] > 0) ? $criteria['jurusan_id'] : 0,
        'mata_pelajaran_id'   => (isset($criteria['mata_pelajaran_id']) && $criteria['mata_pelajaran_id'] > 0) ? $criteria['mata_pelajaran_id'] : 0,
      );

      $proses_penilaian = $this->process_jawaban($criteria['hasil'], $criteria_soal);

      $insert = array(
        'siswa_id_hasil_ujian'            => (isset($criteria['siswa_id']) && $criteria['siswa_id'] > 0) ? $criteria['siswa_id'] : 0,
        'kelas_id_hasil_ujian'            => (isset($criteria['kelas_id']) && $criteria['kelas_id'] > 0) ? $criteria['kelas_id'] : 0,
        'jurusan_id_hasil_ujian'          => (isset($criteria['jurusan_id']) && $criteria['jurusan_id'] > 0) ? $criteria['jurusan_id'] : 0,
        'mata_pelajaran_id_hasil_ujian'   => (isset($criteria['mata_pelajaran_id']) && $criteria['mata_pelajaran_id'] > 0) ? $criteria['mata_pelajaran_id'] : 0,
        'jawaban_hasil_ujian'             => json_encode($proses_penilaian['hasil']),
        'final_hasil_ujian'               => json_encode($proses_penilaian['final']),
      );
  
      $query = $this->model_hasil_ujian::insert($insert);

      if ($query) 
      {
        $result = true;
      }
    }

    return $result;
  }

  function process_jawaban($data, $criteria_soal)
  {
    $result = array();
    $jawaban = array();

    if (count($data) > 0) 
    {
      foreach ($data as $key => $value) 
      {
        if ($value['jawaban'] !== null && $value['jawaban'] !== "undefined" && $value['jawaban'] !== "" && count($value['jawaban'])) 
        {
          $result['hasil'][] = array(
            'nomor_soal'  => $value['nomor_soal'],
            'jawaban'  => $value['jawaban']['value']
          );

        }
        $jawaban[] = array(
          'nomor_soal'  => $value['nomor_soal'],
          'jawaban'     => (isset($value['jawaban']['value']) ? $value['jawaban']['value'] : "null")
        );
      }
      $result['final'] = $this->penilaian($jawaban, $criteria_soal);
    }

    return $result;
  }

  function penilaian($data, $criteria_soal)
  {
    $result = array();
    /* Cari soal nya */
    $soal = $this->model_soal::select("kunci_jawaban_soal", "nomor_soal")->where("mata_pelajaran_id_soal", $criteria_soal['mata_pelajaran_id'])
                              ->where("kelas_id_soal", $criteria_soal['kelas_id'])
                              ->where("jurusan_id_soal", $criteria_soal['jurusan_id'])
                              ->orderBy("nomor_soal", "ASC")
                              ->get();

    if (count($soal)) 
    {
      $soal = json_decode(json_encode($soal), true);
      $jawaban_benar = 0;
      $jawaban_salah = 0;
      $jawaban_kosong = 0;

      $total_soal = $this->model_soal::select("kunci_jawaban_soal", "nomor_soal")->where("mata_pelajaran_id_soal", $criteria_soal['mata_pelajaran_id'])
                              ->where("kelas_id_soal", $criteria_soal['kelas_id'])
                              ->where("jurusan_id_soal", $criteria_soal['jurusan_id'])
                              ->orderBy("nomor_soal", "ASC")
                              ->count();
      
      foreach ($soal as $key => $s) 
      {
        foreach ($data as $key_d => $d) 
        {
          if ($s['nomor_soal'] == $d['nomor_soal']) 
          {
            if ($d['jawaban'] == $s['kunci_jawaban_soal']) 
            {
              $jawaban_benar++;
            }
            else if ($d['jawaban'] == "null") 
            {
              $jawaban_kosong++;
            }
            else if($d['jawaban'] !== $s['kunci_jawaban_soal'])
            {
              $jawaban_salah++;
            }
          }
        }
      }

      $result = array(
        'jawaban_benar'       => $jawaban_benar,
        'jawaban_salah'       => $jawaban_salah,
        'jawaban_kosong'      => $jawaban_kosong,
        'nilai'               => (100 / $total_soal) * $jawaban_benar
      );
    }

    return $result;
  }
}