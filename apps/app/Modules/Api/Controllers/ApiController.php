<?php

namespace App\Modules\Api\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Api\ApiHelper as modul_helper;

class ApiController extends Controller
{
    var $helper;

    public function __construct()
    {
        $this->allow_CORS();
        $this->helper = new modul_helper();
    }

    public function allow_CORS()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) 
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') 
        {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        {
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        }

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        {
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }

        exit(0);
        }
    }
    
    public function siswa_login()
    {
        $post_data  = file_get_contents("php://input");
        $criteria       = json_decode($post_data, TRUE);

        $status     = 500;
		$msg        = "Akun anda tidak terdaftar!";
        $data       = array();
        
        $process_login = $this->helper->siswa_login($criteria);

        if ($process_login['status'] == 200) 
        {
            $status     = 200;
            $msg        = "Berhasil Login";
            $data       = $process_login['data'];
        }
        else if ($process_login['status'] == 401) 
        {
            $status     = 401;
            $msg        = "Detail yg anda masukan salah!";
        }

        $response  = array(
            "status"    => $status,
            "msg"       => $msg,
            "data"      => $data,
        );

        return response()->json($response);
    }

    public function find_mata_pelajaran()
    {
        $post_data  = file_get_contents("php://input");
        $criteria       = json_decode($post_data, TRUE);

        $status     = 500;
		$msg        = "Something went wrong!";
        $data       = array();

        $find = $this->helper->find_mata_pelajaran($criteria);

        if (count($find) > 0) 
        {
            $status     = 200;
            $msg        = "Berhasil";
            $data       = $find;
        }
        else if(count($find) == 0)
        {
            $status     = 200;
            $msg        = "Tidak Ada Data";
        }

        $response  = array(
            "status"    => $status,
            "msg"       => $msg,
            "data"      => $data,
        );

        return response()->json($response);
    }

    public function soal_by_mata_pelajaran()
    {
        $post_data  = file_get_contents("php://input");
        $criteria       = json_decode($post_data, TRUE);

        $status     = 500;
		$msg        = "Something went wrong!";
        $data       = array();

        $find = $this->helper->soal_by_mata_pelajaran($criteria);

        if (count($find) > 0) 
        {
            $status     = 200;
            $msg        = "Berhasil";
            $data       = $find;
        }
        else if(count($find) == 0)
        {
            $status     = 200;
            $msg        = "Tidak Ada Data";
        }

        $response  = array(
            "status"    => $status,
            "msg"       => $msg,
            "data"      => $data,
        );

        return response()->json($response);
    }

    public function simpan_hasil_pekerjaan()
    {
        $post_data  = file_get_contents("php://input");
        $criteria       = json_decode($post_data, TRUE);

        $status     = 500;
		$msg        = "Something went wrong!";
        $data       = array();

        $find = $this->helper->simpan_hasil_pekerjaan($criteria);

        if ($find) 
        {
            $status     = 200;
            $msg        = "Berhasil";
            $data       = $find;
        }

        $response  = array(
            "status"    => $status,
            "msg"       => $msg,
            "data"      => $data,
        );

        return response()->json($response);
    }
}
