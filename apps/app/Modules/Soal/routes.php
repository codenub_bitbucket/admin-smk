<?php

Route::group(['module' => 'Soal', 'middleware' => ['web'], 'namespace' => 'App\Modules\Soal\Controllers'], function() {

    Route::get('soal', 'SoalController@daftar_soal');
    Route::get('soal/tambah', 'SoalController@tambah_soal');
    Route::post('soal/tambah/query_tambah', 'SoalController@query_tambah_soal');
    Route::get('soal/soal-terakhir', 'SoalController@soal_terakhir');
    Route::get('soal/detail/{id}', 'SoalController@detail_soal_mapel');
    Route::get('soal/edit/{id}', 'SoalController@edit_soal');
    Route::post('soal/edit/query_edit', 'SoalController@query_edit_soal');
    Route::get('soal/hapus/{mapel_id}/{id}', 'SoalController@hapus_soal');

});
