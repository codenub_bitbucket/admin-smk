<?php
namespace App\Modules\Soal;

use Illuminate\Support\Facades\DB;

class SoalHelper
{
  var $model_soal        = 'App\Modules\Soal\Models\Soal';
  var $model_jurusan        = 'App\Modules\Jurusan\Models\Jurusan';
	var $model_kelas        = 'App\Modules\Kelas\Models\Kelas';
	var $model_mapel        = 'App\Modules\MataPelajaran\Models\MataPelajaran';

  function query_tambah_soal($data)
  {
    $result = false;
    $insert = array(
      'nomor_soal'              => $data['nomor_soal'],
      'deskripsi_soal'          => $data['deskripsi_soal'],
      'mata_pelajaran_id_soal'  => $data['mata_pelajaran_id_soal'],
      'kelas_id_soal'           => $data['kelas_id_soal'],
      'jurusan_id_soal'         => $data['jurusan_id_soal'],
      'kunci_jawaban_soal'         => $data['kunci_jawaban_soal'],
      'jawaban_soal'            => json_encode(array(
                                      'a' => $data['a'],
                                      'b' => $data['b'],
                                      'c' => $data['c'],
                                      'd' => $data['d'],
                                      'e' => $data['e'],
                                  )),
      'files'                   => json_encode($this->proses_file($data['deskripsi_soal'])),
    );

    $query = $this->model_soal::insert($insert);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function proses_file($files)
  {
    $result = array();
    


    $dom = new \domdocument();
		@$dom->loadHtml($files, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $images = $dom->getelementsbytagname('img');

    foreach($images as $k => $img){
			$data = $img->getattribute('src');

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$image_name= time().$k.'.png';
      $path = base_path() .'/gambar_soal/'. $image_name;
      $result[] = $path;

      if(!is_dir(base_path() .'/gambar_soal'))
      {
          mkdir(base_path() .'/gambar_soal');
      }

			file_put_contents($path, $data);

			$img->removeattribute('src');
			$img->setattribute('src', $image_name);
    }
    
    return $result;
  }

  function kelas_by_kode($kode)
  {
    $result = 0;
    $query  = $this->model_kelas::where('kode_kelas', "=", $kode)
                                ->first();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);
      
      $result = $query['serial_id_kelas'];
    }

    return $result;
  }

  function list_kelas()
  {
    $result = array();

    $query = $this->model_kelas::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function list_jurusan()
  {
    $result = array();

    $query = $this->model_jurusan::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function soal_terkhir($criteria)
  {
    $result = array("nomor_soal" => 0);
    $query = $this->model_soal::select(DB::raw("CONVERT(nomor_soal , INT) as nomor_soal"))->where("mata_pelajaran_id_soal", $criteria['mata_pelajaran_id'])
                              ->where("kelas_id_soal", $criteria['kelas_id'])
                              ->where("jurusan_id_soal", $criteria['jurusan_id'])
                              ->orderBy('nomor_soal', "DESC")
                              ->first();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);
      $result['nomor_soal'] = $query['nomor_soal'];
    }

    return $result;
  }

  function daftar_data()
  {
    $result = array();
    $query = $this->model_mapel::select(DB::raw("count(s.mata_pelajaran_id_soal) as total_soal"), "mata_pelajaran.serial_id_mata_pelajaran", "mata_pelajaran.nama_mata_pelajaran", "k.nomor_kelas", "k.nama_kelas", "j.nama_jurusan")
                                ->leftjoin('soal as s', "s.mata_pelajaran_id_soal", "=", "mata_pelajaran.serial_id_mata_pelajaran")
                                ->leftjoin("kelas as k", "k.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->leftjoin("jurusan as j", "j.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->groupBy("mata_pelajaran.serial_id_mata_pelajaran")
                                ->get();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);

      $result = $query;
    }

    return $result;
  }

  function detail_soal_mapel($id)
  {
    $result = array();
    $query = $this->model_mapel::select("s.*", "mata_pelajaran.serial_id_mata_pelajaran")
                                ->where("mata_pelajaran.serial_id_mata_pelajaran", $id)
                                ->rightjoin('soal as s', "s.mata_pelajaran_id_soal", "=", "mata_pelajaran.serial_id_mata_pelajaran")
                                ->leftjoin("kelas as k", "k.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->leftjoin("jurusan as j", "j.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->orderBy("s.nomor_soal", "ASC")
                                ->get();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);

      $result = $query;
    }

    return $result;
  }

  function detail_soal_by_id($id)
  {
    $result = array();
    $query = $this->model_mapel::select("s.*")
                                ->where("s.serial_id_soal", $id)
                                ->rightjoin('soal as s', "s.mata_pelajaran_id_soal", "=", "mata_pelajaran.serial_id_mata_pelajaran")
                                ->leftjoin("kelas as k", "k.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->leftjoin("jurusan as j", "j.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->first();

    if (count($query)) 
    {
      $query = json_decode(json_encode($query), true);

      $result = $query;
    }

    return $result;
  }

  function query_edit_soal($data)
  {
    $result = false;
    $update = array(
      'deskripsi_soal'          => $data['deskripsi_soal'],
      'kunci_jawaban_soal'         => $data['kunci_jawaban_soal'],
      'jawaban_soal'            => json_encode(array(
                                      'a' => $data['a'],
                                      'b' => $data['b'],
                                      'c' => $data['c'],
                                      'd' => $data['d'],
                                      'e' => $data['e'],
                                  )),
      'files'                   => json_encode($this->proses_file($data['deskripsi_soal'])),
    );

    $query = $this->model_soal::where('serial_id_soal', $data['serial_id_soal'])->update($update);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function hapus_soal($id)
  {
    $criteria_soal = $this->model_soal::where("serial_id_soal", $id)->first();
    $query = $this->model_soal::where("serial_id_soal", $id)->delete();
    $result = false;

    if ($query) 
    {
      /* Susun ulang nomor soal */
      $criteria_soal = json_decode(json_encode($criteria_soal), true);
      $get = $this->model_soal::select("serial_id_soal", "nomor_soal")
                                ->where("mata_pelajaran_id_soal", $criteria_soal['mata_pelajaran_id_soal'])
                                ->where("kelas_id_soal", $criteria_soal['kelas_id_soal'])
                                ->where("jurusan_id_soal", $criteria_soal['jurusan_id_soal'])
                                ->get();
      if (count($get)) 
      {
        $get = json_decode(json_encode($get), true);
        echo "<pre>"; print_r($get); 
        foreach ($get as $key => $value) 
        {
          $c_n = $value['nomor_soal'];
          if ((int) $c_n > (int) $criteria_soal['nomor_soal']) 
          {
            $update['nomor_soal'] = (int) $c_n - 1;
            $susun_ulang = $this->model_soal::where("serial_id_soal", $value['serial_id_soal'])->update($update);
          }
        }
      }
      $result = true;
    }

    return $result;
  }
}