<?php

namespace App\Modules\Soal\Models;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model {

    protected $table 		= "soal";
	protected $primaryKey 	= "serial_id_soal";
	protected $guarded = array('serial_id_soal');
	public $timestamps = false;

}
