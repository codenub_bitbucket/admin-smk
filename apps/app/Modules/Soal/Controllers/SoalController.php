<?php

namespace App\Modules\Soal\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Soal\SoalHelper as module_helper;

class SoalController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function daftar_soal()
    {
        $helper = new module_helper();
        $daftar_data = $helper->daftar_data();
        $view = 'Soal::index';

		$view_data = array(
			'daftar_data'              => $daftar_data,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    public function tambah_soal()
    {
        $helper = new module_helper();

        $list_kelas = $helper->list_kelas();
        $list_jurusan = $helper->list_jurusan();

        $view = 'Soal::create';

		$view_data = array(
            'page'        => "create",
            'list_kelas' => $list_kelas,
            'list_jurusan' => $list_jurusan,
		);
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
		}
    }

    public function query_tambah_soal(Request $request)
    {
        $helper = new module_helper();
        $data = $request->all();
        $tambah = $helper->query_tambah_soal($data);
        if ($tambah) 
        {
            $result     = array(
                'status'     => 200,
                'msg'        => "Berhasil Menambahkan Soal"
            );
            return response()->json($result);
        }
        else
        {
            $result     = array(
                'status'     => 500,
                'msg'        => "Gagal Menambahkan Soal"
            );
            return response()->json($result);
        }
    }

    public function soal_terakhir(Request $request)
    {
        $helper = new module_helper();
        $soal_terkhir = $helper->soal_terkhir($request->all());

		return $soal_terkhir;
    }

    public function detail_soal_mapel($id)
    {
        $helper = new module_helper();
        $id = base64_decode($id);
        
        $detail_soal = $helper->detail_soal_mapel($id);
        if (count($detail_soal) == 0) 
        {
            return redirect('soal');
        }
        
        $view = 'Soal::detail';
		
		$view_data = array(
            'detail_soal' => $detail_soal,
		);
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
		}
    }

    public function edit_soal($id)
    {
        $helper = new module_helper();
        $id = base64_decode($id);
        
        $data = $helper->detail_soal_by_id($id);
        if (count($data) == 0) 
        {
            return redirect('soal');
        }
        
        $data['jawaban_soal'] = json_decode($data['jawaban_soal'], true); 

        $list_kelas = $helper->list_kelas();
        $list_jurusan = $helper->list_jurusan();
        
        $view = 'Soal::create';
		
		$view_data = array(
            'page'        => "edit",
            'data' => $data,
            'list_kelas' => $list_kelas,
            'list_jurusan' => $list_jurusan,
		);
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
		}
    }

    public function query_edit_soal(Request $request)
    {
        $helper = new module_helper();
        $data = $request->all();
        $edit = $helper->query_edit_soal($data);
        if ($edit) 
        {
            $result     = array(
                'status'     => 200,
                'msg'        => "Berhasil Edit Soal"
            );
            return response()->json($result);
        }
        else
        {
            $result     = array(
                'status'     => 500,
                'msg'        => "Gagal Edit Soal"
            );
            return response()->json($result);
        }
    }

    public function hapus_soal($mapel_id , $id)
    {
        $helper = new module_helper();
		$id = base64_decode($id);

        $hapus = $helper->hapus_soal($id);

        if ($hapus) 
        {
            return redirect('soal/detail/'.$mapel_id)->with('message', "Soal Berhasil Dihapus.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Menghapus Soal!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }

}
