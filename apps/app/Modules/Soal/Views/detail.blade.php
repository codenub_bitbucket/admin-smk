@extends('layouts.app')

@section('title', 'Detail Soal')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item"><a href="{{ URL('soal') }}">Soal</a></li>
<li class="breadcrumb-item active"><a href="#">Detail</a></li>
@endsection

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Detail Soal</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Nomor</th>
              <th>Pertanyaan</th>
              <th>Pilihan</th>
              <th>Jawaban</th>
              <th>###</th>
            </tr>
          </thead>
          <tbody>
            @foreach($detail_soal as $key => $value)
            <tr>
              <td style="width:65px">{{ $value['nomor_soal'] }}</td>
              <td style="width:550px">{!! nl2br($value['deskripsi_soal']) !!}</td>
              <td>
                <ul style="padding: 0px;margin:0px">
                  @foreach(json_decode($value['jawaban_soal']) as $k => $a)
                  <li style="list-style-type:none">{{ strtoupper($k) }}. {{ $a }}</li>
                  @endforeach
                </ul>
              </td>
              <td style="width:65px">{{ strtoupper($value['kunci_jawaban_soal']) }}</td>
              <td>
                <a class="btn btn-primary btn-block" href="{{ URL('soal/edit/'.base64_encode($value['serial_id_soal'])) }}">Edit</a>
                <a class="btn btn-danger btn-block" href="{{ URL('soal/hapus/'.base64_encode($value['serial_id_mata_pelajaran']).'/'.base64_encode($value['serial_id_soal'])) }}">Hapus</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</section>

<script>
  if ("{{isset($_GET['_berhasil']) && $_GET['_berhasil']}}")
  {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000
    });
    
    Toast.fire({
      icon: "success",
      title: " {{ isset($_GET['_berhasil']) ? base64_decode($_GET['_berhasil']) : '' }} "
    })
  }
</script>
@if (Session::has('message'))
  <script>
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000
    });
    
    Toast.fire({
      icon: "{{ Session::get('class') }}",
      title: " {{ Session::get('message') }} "
    })
  </script>
@endif
@endsection