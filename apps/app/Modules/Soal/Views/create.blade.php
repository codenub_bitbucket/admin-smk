@extends('layouts.app')

@if($page == "edit")
  @section('title', 'Edit Soal')
@else
  @section('title', 'Tambah Soal')
@endif

@if($page == "edit")
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('soal') }}">Soal</a></li>
  <li class="breadcrumb-item active"><a href="#">Edit</a></li>
  @endsection
@else
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('soal') }}">Soal</a></li>
  <li class="breadcrumb-item active"><a href="#">Tambah</a></li>
  @endsection
@endif

@section('content')
<div class="">
  <form method="POST" onsubmit="event.preventDefault(); validateMyForm();">
  {{ csrf_field() }}
    <div class="card card-primary card-outline">
      <div class="card-header">
        @if($page == "edit")
        <h3 class="card-title">Formulir Edit Soal</h3>
        @else
        <h3 class="card-title">Formulir Tambah Soal</h3>
        @endif
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if($page == "edit")
        <input type="hidden" name="serial_id_soal" value="{{ $data['serial_id_soal'] }}">
        @endif
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Kelas <span style="color:red">*</span></h6>
          @if($page == "edit")
            <select class="form-control kelas_id_soal" name="kelas_id_soal" disabled="true">
              <option value="">- Pilih Kelas -</option>
              @foreach($list_kelas as $key => $row)
              <option 
              {{ ($data['kelas_id_soal'] == $row['serial_id_kelas']) ? 'selected' : '' }}
              value="{{ $row['serial_id_kelas'] }}">{{ $row['nomor_kelas'] }} ( {{ $row['nama_kelas'] }} )</option>
              @endforeach
            </select>
          @else
            <select class="form-control kelas_id_soal" name="kelas_id_soal">
              <option value="">- Pilih Kelas -</option>
              @foreach($list_kelas as $key => $row)
              <option value="{{ $row['serial_id_kelas'] }}">{{ $row['nomor_kelas'] }} ( {{ $row['nama_kelas'] }} )</option>
              @endforeach
            </select>
          @endif
        </div>
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Jurusan <span style="color:red">*</span></h6>
          @if($page == "edit")
          <select class="form-control jurusan_id_soal" name="jurusan_id_soal" disabled="true">
            <option value="">- Pilih Jurusan -</option>
            @foreach($list_jurusan as $key => $row)
            <option 
            {{ ($data['jurusan_id_soal'] == $row['serial_id_jurusan']) ? 'selected' : '' }}
            value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
            @endforeach
          </select>
          @else
          <select class="form-control jurusan_id_soal" name="jurusan_id_soal" disabled="true">
            <option value="">- Pilih Jurusan -</option>
            @foreach($list_jurusan as $key => $row)
            <option value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
            @endforeach
          </select>
          @endif
        </div>
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Mata Pelajaran <span style="color:red">*</span></h6>
          <select class="form-control mata_pelajaran_id_soal" name="mata_pelajaran_id_soal" disabled="true">
          </select>
        </div>
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Nomor Soal</h6>
          @if($page == "edit")
          <input class="form-control nomor_soal" name="nomor_soal" value="{{ $data['nomor_soal'] }}" readonly>
          @else
          <input class="form-control nomor_soal" name="nomor_soal" readonly>
          @endif
        </div>
        @if($page == "create")
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Detail Soal</h6>
          <input class="form-control detail_soal" disabled>
        </div>
        @endif
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Pertanyaan <span style="color:red">*</span></h6>
          @if($page == "edit")
          <textarea name="deskripsi_soal" id="compose-textarea" class="form-control" style="height: 300px" required autocomplete="off">{{ $data['deskripsi_soal'] }}</textarea>
          @else
          <textarea name="deskripsi_soal" id="compose-textarea" class="form-control" style="height: 300px" required> </textarea>
          @endif
        </div>

        <h5>Jawaban</h5>
        <div class="row">
          <div class="col-lg-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  A
                </span>
              </div>
              @if($page == "edit")
              <input type="text" class="form-control" name="a" value="{{ $data['jawaban_soal']['a'] }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="a">
              @endif
            </div>
            <!-- /input-group -->
          </div>
          <div class="col-lg-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  B
                </span>
              </div>
              @if($page == "edit")
              <input type="text" class="form-control" name="b" value="{{ $data['jawaban_soal']['b'] }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="b">
              @endif
            </div>
            <!-- /input-group -->
          </div>
          <div class="col-lg-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  C
                </span>
              </div>
              @if($page == "edit")
              <input type="text" class="form-control" name="c" value="{{ $data['jawaban_soal']['c'] }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="c">
              @endif
            </div>
            <!-- /input-group -->
          </div>
          <div class="col-lg-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  D
                </span>
              </div>
              @if($page == "edit")
              <input type="text" class="form-control" name="d" value="{{ $data['jawaban_soal']['d'] }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="d">
              @endif
            </div>
            <!-- /input-group -->
          </div>
          <div class="col-lg-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  E
                </span>
              </div>
              @if($page == "edit")
              <input type="text" class="form-control" name="e" value="{{ isset($data['jawaban_soal']['e']) ? $data['jawaban_soal']['e'] : '' }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="e">
              @endif
            </div>
            <!-- /input-group -->
          </div>
        </div>
        <!-- /.row -->
        <br>
        <hr>
        <div class="form-group">
          <h6 style="padding: 0; margin:0;">Kunci Jawaban <span style="color:red">*</span></h6>
          @if($page == "edit")
          <select class="form-control" name="kunci_jawaban_soal">
            <option value="">- Pilih Jawaban -</option>
            <option value="a" {{ ($data['kunci_jawaban_soal'] == 'a') ? 'selected' : '' }}>A</option>
            <option value="b" {{ ($data['kunci_jawaban_soal'] == 'b') ? 'selected' : '' }}>B</option>
            <option value="c" {{ ($data['kunci_jawaban_soal'] == 'c') ? 'selected' : '' }}>C</option>
            <option value="d" {{ ($data['kunci_jawaban_soal'] == 'd') ? 'selected' : '' }}>D</option>
            <option value="e" {{ ($data['kunci_jawaban_soal'] == 'e') ? 'selected' : '' }}>E</option>
          </select>
          @else
          <select class="form-control" name="kunci_jawaban_soal">
            <option value="">- Pilih Jawaban -</option>
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
            <option value="d">D</option>
            <option value="e">E</option>
          </select>
          @endif
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <div class="float-right">
          <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan Soal</button>
        </div>
        <button class="btn btn-default"><i class="fas fa-window-close"></i> Batalkan</button>
      </div>
      <!-- /.card-footer -->
    </div>
  </form>
</div>

@if($page == "edit")
<script>

  function validateMyForm()
  {
    var post_form = $('form').serialize();
    var token = "{{ csrf_token() }}";

    var data_form = [];
    data_form['jawaban_soal'] = [];
    var temp_form = $('form').serializeArray().map(function(v) {
                                                    if (v.name == "a" || v.name == "b" || v.name == "c" || v.name == "d") 
                                                    {
                                                      data_form['jawaban_soal'][v.name] = v.value;  
                                                    }
                                                    else
                                                    {
                                                      data_form[v.name] = v.value
                                                    }
                                                  });
    var form = [
      'deskripsi_soal',
      'jurusan_id_soal',
      'kelas_id_soal',
      'mata_pelajaran_id_soal',
      'kunci_jawaban_soal',
    ];

    var i = 0;
    var incomplate = false;
    form.forEach(element => 
    {
      i++;
      if (data_form[element] == "" || data_form[element] == "<br>" || data_form[element] == " " || data_form[element] == "<p><br></p>" || data_form[element] == "&nbsp;") 
      {
        incomplate = true;
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 5000
        });
        
        Toast.fire({
          icon: "error",
          title: "Isi Semua Inputan Yang Bertanda ( * )"
        })
        return false;
      }  
      
      if (i == form.length && incomplate == false) 
      {
        Swal.fire({
          title: 'Menambahkan Soal',
          html: 'Sedang Menyimpan Soal. Mohon tunggu...',
          onBeforeOpen: () => {
            Swal.showLoading()
          },
        });

        $.ajax({
            url: "{{ ($page == 'edit') ? URL('soal/edit/query_edit') : URL('soal/tambah/query_tambah') }}",
            type: 'POST',
            data: "data="+post_form+"&_token=" + token,
            dataType: 'JSON',
            success: function (data) { 
              Swal.close();
                Swal.fire({
                  position: 'middle',
                  icon: 'success',
                  title: 'Your work has been saved',
                  showConfirmButton: false,
                  timer: 1500
                });
                let timerInterval
                Swal.fire({
                  title: 'Mohon tunggu',
                  html: 'Anda akan diarahkan kembali dalam <b></b> milidetik.',
                  timer: 2000,
                  timerProgressBar: true,
                  onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                      const content = Swal.getContent()
                      if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                          b.textContent = Swal.getTimerLeft()
                        }
                      }
                    }, 100)
                  },
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                }).then((result) => {
                  /* Read more about handling dismissals below */
                  if (result.dismiss === Swal.DismissReason.timer) 
                  {
                    if ("{{ $page == 'edit' }}") 
                    {
                      window.location = "{{ URL('soal/detail/'.base64_encode($data['mata_pelajaran_id_soal']).'?_berhasil='.base64_encode('Berhasil Edit Soal')) }}";
                    }
                    else
                    {
                      window.location = "{{ URL('soal') }}";
                    }
                  }
                })
            }
        }); 
      }
    });
  }

  if ("{{$page}}" == "edit") 
  {
    $(function () {
      var kelas = "{{ $data['kelas_id_soal'] }}";
      var jurusan = "{{ $data['jurusan_id_soal'] }}";
      $.ajax({
          url: "{{ URL('mata-pelajaran/cari') }}",
          type: 'GET',
          data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
          dataType: 'JSON',
          success: function (data) { 
            if (data.length > 0) 
            {
              $.each(data,function(key, value)
              {
                var r = "{{ $data['mata_pelajaran_id_soal'] }}";
                var d = value.serial_id_mata_pelajaran;
                if (r == d) 
                {
                  $(".mata_pelajaran_id_soal").append('<option selected value=' + value.serial_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
                }
              });  
            }
          }
      }); 
    })
  }

  $('.kelas_id_soal').on('change', function() {
    $('.jurusan_id_soal').prop("disabled", false);
  });

  $('.jurusan_id_soal').on('change', function() {
    var kelas = $('.kelas_id_soal').val();
    var jurusan = $('.jurusan_id_soal').val();
    $.ajax({
        url: "{{ URL('mata-pelajaran/cari') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
        dataType: 'JSON',
        success: function (data) { 
            
          if (data.length > 0) 
          {
            $(".mata_pelajaran_id_soal").prop("disabled", false);
            $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
            $.each(data,function(key, value)
            {
                $(".mata_pelajaran_id_soal").append('<option value=' + value.serial_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
            });  
          }
          else
          {
            $(".mata_pelajaran_id_soal").prop("disabled", true);
            $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
          }
            
        }
    }); 
  });

  $('.kelas_id_soal').on('change', function() {
    if ($('.jurusan_id_soal').val() !== "" && $('.jurusan_id_soal').val() !== undefined) 
    {
      var kelas = $('.kelas_id_soal').val();
      var jurusan = $('.jurusan_id_soal').val();
      $.ajax({
          url: "{{ URL('mata-pelajaran/cari') }}",
          type: 'GET',
          data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
          dataType: 'JSON',
          success: function (data) { 
              
            if (data.length > 0) 
            {
              $(".mata_pelajaran_id_soal").prop("disabled", false);
              $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
              $.each(data,function(key, value)
              {
                  $(".mata_pelajaran_id_soal").append('<option value=' + value.jurusan_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
              });  
            }
            else
            {
              $(".mata_pelajaran_id_soal").prop("disabled", true);
              $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
            }
              
          }
      });   
    }
  });

  $('.mata_pelajaran_id_soal').on('change', function() {
    var kelas = $('.kelas_id_soal').val();
    var jurusan = $('.jurusan_id_soal').val();
    var mapel = $('.mata_pelajaran_id_soal').val();
    $.ajax({
        url: "{{ URL('soal/soal-terakhir') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}&mata_pelajaran_id=${mapel}`,
        dataType: 'JSON',
        success: function (data) { 

          let k = $(".kelas_id_soal option:selected").text();
          let j = $(".jurusan_id_soal option:selected").text();
          let m = $(".mata_pelajaran_id_soal option:selected").text();
            
          if (mapel !== "") 
          {
            $('.nomor_soal').val(parseInt(data['nomor_soal']) + 1);
            $('.detail_soal').val(`Soal ${m} Kelas ${k} ${j} Nomor ${parseInt(data['nomor_soal']) + 1}`);
          }
          else
          {
            $('.nomor_soal').val("");
            $('.detail_soal').val("");
          }
            
        }
    }); 
  });

  $(function () {
    //Add text editor
    $('#compose-textarea').summernote({
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['picture']
      ]
    });
  })
</script>
@else
<script>

  function validateMyForm()
  {
    var post_form = $('form').serialize();
    var token = "{{ csrf_token() }}";

    var data_form = [];
    data_form['jawaban_soal'] = [];
    var temp_form = $('form').serializeArray().map(function(v) {
                                                    if (v.name == "a" || v.name == "b" || v.name == "c" || v.name == "d") 
                                                    {
                                                      data_form['jawaban_soal'][v.name] = v.value;  
                                                    }
                                                    else
                                                    {
                                                      data_form[v.name] = v.value
                                                    }
                                                  });
    var form = [
      'deskripsi_soal',
      'jurusan_id_soal',
      'kelas_id_soal',
      'mata_pelajaran_id_soal',
      'kunci_jawaban_soal',
    ];

    var i = 0;
    var incomplate = false;
    form.forEach(element => 
    {
      i++;
      if (data_form[element] == "" || data_form[element] == "<br>" || data_form[element] == " " || data_form[element] == "<p><br></p>" || data_form[element] == "&nbsp;") 
      {
        incomplate = true;
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 5000
        });
        
        Toast.fire({
          icon: "error",
          title: "Isi Semua Inputan Yang Bertanda ( * )"
        })
        return false;
      }  
      
      if (i == form.length && incomplate == false) 
      {
        Swal.fire({
          title: 'Menambahkan Soal',
          html: 'Sedang Menyimpan Soal. Mohon tunggu...',
          onBeforeOpen: () => {
            Swal.showLoading()
          },
        });

        $.ajax({
            url: "{{ URL('soal/tambah/query_tambah') }}",
            type: 'POST',
            data: "data="+post_form+"&_token=" + token,
            dataType: 'JSON',
            success: function (data) { 
              if (data['status'] == 200) 
              {
                Swal.close();
                Swal.fire({
                  position: 'middle',
                  icon: 'success',
                  title: 'Your work has been saved',
                  showConfirmButton: false,
                  timer: 1500
                });
                let timerInterval
                Swal.fire({
                  title: 'Mohon tunggu',
                  html: 'Anda akan diarahkan kembali dalam <b></b> milidetik.',
                  timer: 2000,
                  timerProgressBar: true,
                  onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                      const content = Swal.getContent()
                      if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                          b.textContent = Swal.getTimerLeft()
                        }
                      }
                    }, 100)
                  },
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                }).then((result) => {
                  /* Read more about handling dismissals below */
                  if (result.dismiss === Swal.DismissReason.timer) 
                  {
                    window.location = "{{ URL('soal?_berhasil='.base64_encode('Berhasil Menambahkan Soal')) }}";
                  }
                })
              }
            }
        }); 
      }
    });
    
    // if()
    // { 
    //   alert("validation failed false");
    //   returnToPreviousPage();
    //   return false;
    // }

    // alert("validations passed");
    // return true;
  }

  $('.kelas_id_soal').on('change', function() {
    $('.jurusan_id_soal').prop("disabled", false);
  });

  $('.jurusan_id_soal').on('change', function() {
    var kelas = $('.kelas_id_soal').val();
    var jurusan = $('.jurusan_id_soal').val();
    $.ajax({
        url: "{{ URL('mata-pelajaran/cari') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
        dataType: 'JSON',
        success: function (data) { 
            
          if (data.length > 0) 
          {
            $(".mata_pelajaran_id_soal").prop("disabled", false);
            $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
            $.each(data,function(key, value)
            {
                $(".mata_pelajaran_id_soal").append('<option value=' + value.serial_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
            });  
          }
          else
          {
            $(".mata_pelajaran_id_soal").prop("disabled", true);
            $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
          }
            
        }
    }); 
  });

  $('.kelas_id_soal').on('change', function() {
    if ($('.jurusan_id_soal').val() !== "" && $('.jurusan_id_soal').val() !== undefined) 
    {
      var kelas = $('.kelas_id_soal').val();
      var jurusan = $('.jurusan_id_soal').val();
      $.ajax({
          url: "{{ URL('mata-pelajaran/cari') }}",
          type: 'GET',
          data: `kelas_id=${kelas}&jurusan_id=${jurusan}`,
          dataType: 'JSON',
          success: function (data) { 
              
            if (data.length > 0) 
            {
              $(".mata_pelajaran_id_soal").prop("disabled", false);
              $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Pilih mata pelajaran -</option>');
              $.each(data,function(key, value)
              {
                  $(".mata_pelajaran_id_soal").append('<option value=' + value.jurusan_id_mata_pelajaran + '>' + value.nama_mata_pelajaran + '</option>');
              });  
            }
            else
            {
              $(".mata_pelajaran_id_soal").prop("disabled", true);
              $(".mata_pelajaran_id_soal").find('option').remove().end().append('<option value="">- Tidak ada mata pelajaran -</option>');
            }
              
          }
      });   
    }
  });

  $('.mata_pelajaran_id_soal').on('change', function() {
    var kelas = $('.kelas_id_soal').val();
    var jurusan = $('.jurusan_id_soal').val();
    var mapel = $('.mata_pelajaran_id_soal').val();
    $.ajax({
        url: "{{ URL('soal/soal-terakhir') }}",
        type: 'GET',
        data: `kelas_id=${kelas}&jurusan_id=${jurusan}&mata_pelajaran_id=${mapel}`,
        dataType: 'JSON',
        success: function (data) { 

          let k = $(".kelas_id_soal option:selected").text();
          let j = $(".jurusan_id_soal option:selected").text();
          let m = $(".mata_pelajaran_id_soal option:selected").text();
            
          if (mapel !== "") 
          {
            $('.nomor_soal').val(parseInt(data['nomor_soal']) + 1);
            $('.detail_soal').val(`Soal ${m} Kelas ${k} ${j} Nomor ${parseInt(data['nomor_soal']) + 1}`);
          }
          else
          {
            $('.nomor_soal').val("");
            $('.detail_soal').val("");
          }
            
        }
    }); 
  });

  $(function () {
    //Add text editor
    $('#compose-textarea').summernote({
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['picture']
      ]
    });
  })
</script>
@endif
@endsection