@extends('layouts.app')

@section('title', 'Soal')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item active"><a href="#">Soal</a></li>
@endsection

@section('content')
<div class="">
  @if( Session::has("success_add") )
  <div class="alert alert-success alert-block" role="alert">
      <button class="close" data-dismiss="alert"></button>
      {{ Session::get("success_add") }}
  </div>
  @endif
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Soal</h3>
      <div class="text-right">
          <a type="button" class="btn btn-primary btn-sm" href="{{ URL('soal/tambah') }}">
            Tambah
          </a>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nama Soal</th>
          <th>Kelas</th>
          <th>Jurusan</th>
          <th>Total Soal</th>
          <th>###</th>
        </tr>
        </thead>
        <tbody>
          @foreach($daftar_data as $key => $value)
          <tr>
            <td>{{ $value['nama_mata_pelajaran'] }}</td>
            <td>{{ $value['nomor_kelas'] }} ({{ $value['nama_kelas'] }})</td>
            <td>{{ $value['nama_jurusan'] }}</td>
            <td>{{ $value['total_soal'] }}</td>
            <td>
              <a class="btn btn-primary btn-sm" href="{{ URL('soal/detail/'.base64_encode($value['serial_id_mata_pelajaran'])) }}">Lihat Soal</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
<script src="{{ URL('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL('assets/plugins/toastr/toastr.min.js') }}"></script>

<script>
  if ("{{isset($_GET['_berhasil']) && $_GET['_berhasil']}}")
  {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000
    });
    
    Toast.fire({
      icon: "success",
      title: " {{ isset($_GET['_berhasil']) ? base64_decode($_GET['_berhasil']) : '' }} "
    })
  }
</script>


@endsection