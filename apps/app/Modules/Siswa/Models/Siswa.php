<?php

namespace App\Modules\Siswa\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model {

    protected $table 		= "siswa";
	protected $primaryKey 	= "serial_id_siswa";
	protected $guarded = array('serial_id_siswa');
	public $timestamps = false;

}
