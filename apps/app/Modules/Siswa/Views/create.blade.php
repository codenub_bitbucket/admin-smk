@extends('layouts.app')

@if($page == "edit")
  @section('title', 'Edit Siswa')
@else
  @section('title', 'Tambah Siswa')
@endif

@if($page == "edit")
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('siswa') }}">Siswa</a></li>
  <li class="breadcrumb-item active"><a href="#">Edit</a></li>
  @endsection
@else
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('siswa') }}">Siswa</a></li>
  <li class="breadcrumb-item active"><a href="#">Tambah</a></li>
  @endsection
@endif


@section('content')
<!-- Content Header (Page header) -->
<div class="">
  <div class="card card-info">
    <div class="card-header">
      @if($page == "edit")
      <h3 class="card-title">Edit Siswa</h3>
      @else
      <h3 class="card-title">Tambah Siswa</h3>
      @endif
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form class="form-horizontal" method="POST" action="{{ ($page == 'edit') ? URL('siswa/edit/query') : URL('siswa/tambah/query') }}">
      {{ csrf_field() }}
      <div class="card-body">
        <div class="form-group row">
          <label for="mata-siswa" class="col-sm-2 col-form-label">Nama</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <input type="hidden" name="serial_id_siswa" value="{{$data['serial_id_siswa']}}">
            <input type="text" class="form-control" name="nama_siswa" id="mata-siswa" placeholder="Nama Siswa" value="{{ $data['nama_siswa'] }}">
            @else
            <input type="text" class="form-control" name="nama_siswa" id="mata-siswa" placeholder="Nama Siswa">
            @endif
          </div>
        </div>
        <div class="form-group row">
          <label for="kelas_id_siswa" class="col-sm-2 col-form-label">Kelas</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <select class="form-control" name="kelas_id_siswa">
              @foreach($list_kelas as $key => $row)
              <option 
              {{ ($data['kelas_id_siswa'] == $row['serial_id_kelas']) ? 'selected' : '' }}
              value="{{ $row['serial_id_kelas'] }}">{{ $row['nomor_kelas'] }}</option>
              @endforeach
            </select>
            @else
            <select class="form-control" name="kelas_id_siswa">
              <option value="">- Pilih Kelas -</option>
              @foreach($list_kelas as $key => $row)
              <option value="{{ $row['serial_id_kelas'] }}">{{ $row['nomor_kelas'] }}</option>
              @endforeach
            </select>
            @endif
          </div>
        </div>
        
        <div class="form-group row">
          <label for="jurusan_id" class="col-sm-2 col-form-label">Jurusan</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <select class="form-control" name="jurusan_id_siswa">
              @foreach($list_jurusan as $key => $row)
              <option 
              {{ ($data['jurusan_id_siswa'] == $row['serial_id_jurusan']) ? 'selected' : '' }}
              value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
              @endforeach
            </select>
            @else
            <select class="form-control" name="jurusan_id_siswa">
              <option value="">- Pilih Jurusan -</option>
              @foreach($list_jurusan as $key => $row)
              <option value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
              @endforeach
            </select>
            @endif
          </div>
        </div>

        <div class="form-group row">
          <label for="sub-jurusan" class="col-sm-2 col-form-label">Sub Jurusan</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <input type="text" class="form-control" name="sub_jurusan_siswa" id="sub-jurusan" placeholder="Sub Jurusan" value="{{ $data['sub_jurusan_siswa'] }}">
            @else
            <input type="text" class="form-control" name="sub_jurusan_siswa" id="sub-jurusan" placeholder="Sub Jurusan">
            @endif
            <small>contoh : A/1 atau B/2 => X TKJ B/2</small>
          </div>
        </div>

        <div class="form-group row">
          <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <input type="text" class="form-control" name="nisn_siswa" id="nisn" placeholder="nisn" value="{{ $data['nisn_siswa'] }}">
            @else
            <input type="text" class="form-control" name="nisn_siswa" id="nisn" placeholder="nisn">
            @endif
          </div>
        </div>

        <div class="form-group row">
          <label for="username" class="col-sm-2 col-form-label">Username</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <input type="text" class="form-control" name="username_siswa" id="username" placeholder="Username" value="{{ $data['username_siswa'] }}">
            @else
            <input type="text" class="form-control" name="username_siswa" id="username" placeholder="Username">
            @endif
          </div>
        </div>

        <div class="form-group row">
          <label for="password" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
            @if($page == "edit")
            <div class="input-group mb-3 password_old">
              <input type="password" class="form-control" name="password_siswa" id="password" placeholder="Password" value="{{ $data['password_siswa'] }}" readonly>
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" onclick="gantipassword()">Ganti Passwod</button>
              </div>
            </div>
            <input type="text" class="form-control password_new" style="display: none;" name="password_siswa" id="password" placeholder="Password">
            @else
            <input type="text" class="form-control" name="password_siswa" id="password" placeholder="Password">
            @endif
          </div>
        </div>

      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <a class="btn btn-danger" href="{{ URL('siswa') }}">Cancel</a>
        @if($page == 'edit')
        <button type="submit" class="btn btn-info float-right">Simpan Perubahan</button>
        @else
        <button type="submit" class="btn btn-info float-right">Tambah</button>
        @endif
      </div>
      <!-- /.card-footer -->
    </form>
  </div>
</div>

<script>
  function gantipassword()
  {
    $('.password_old').hide();
    $('.password_new').css("display", "");
  }
</script>
@endsection