@extends('layouts.app')

@section('title', 'Siswa')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item active"><a href="#">Siswa</a></li>
@endsection

@section('content')
<div class="div">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Siswa</h3>
      <div class="text-right">
        <a type="button" class="btn btn-primary btn-sm" href="{{ URL('siswa/tambah') }}">
          Tambah
        </a>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Jurusan</th>
          <th>Sub Jurusan</th>
          <th>NISN</th>
          <th>Username</th>
          <th>Password</th>
          <th>###</th>
        </tr>
        </thead>
        <tbody>
          @foreach($siswa as $key => $value)
          <tr>
            <td>{{ $value['nama_siswa'] }}</td>
            <td>{{ $value['nomor_kelas'] }} ({{ $value['nama_kelas'] }})</td>
            <td>{{ $value['nama_jurusan'] }}</td>
            <td>{{ $value['sub_jurusan_siswa'] }}</td>
            <td>{{ $value['nisn_siswa'] }}</td>
            <td>{{ $value['username_siswa'] }}</td>
            <td>{{ base64_decode($value['password_siswa']) }}</td>
            <td>
              <a class="btn btn-primary btn-sm" href="{{ URL('siswa/edit/'.base64_encode($value['serial_id_siswa'])) }}">Edit</a>
              <a class="btn btn-danger btn-sm text-white" onclick="hapus_siswa('<?= base64_encode($value['serial_id_siswa']) ?>')">Hapus</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>

<script src="{{ URL('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL('assets/plugins/toastr/toastr.min.js') }}"></script>
@if (Session::has('message'))
  <script>
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 5000
    });
    
    Toast.fire({
      icon: "{{ Session::get('class') }}",
      title: " {{ Session::get('message') }} "
    })
  </script>
@endif
<script>
  function hapus_siswa(id) 
  {
    Swal.fire({
      title: 'Apakah anda yakin?',
      text: "Data Siswa akan terhapus secara permanen!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: "Tidak",
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya'
    }).then((result) => {
      if (result.value) 
      {
        window.location = "{{ URL('siswa/hapus') }}" + "/" + id;
      }
    });
  }
</script>
<script>
  
</script>

@endsection