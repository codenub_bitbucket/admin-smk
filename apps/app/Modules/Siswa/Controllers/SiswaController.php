<?php

namespace App\Modules\Siswa\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Siswa\SiswaHelper as module_helper;

class SiswaController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function daftar_data()
    {
        $helper = new module_helper();
        $daftar_data = $helper->daftar_data();
        $view = 'Siswa::index';

		$view_data = array(
			'siswa'              => $daftar_data,
		);
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
		}
    }

    public function tambah_siswa()
    {
        $helper = new module_helper();

        $list_kelas = $helper->list_kelas();
        $list_jurusan = $helper->list_jurusan();

        $view = 'Siswa::create';

		$view_data = array(
            'page'        => "create",
            'list_kelas' => $list_kelas,
            'list_jurusan' => $list_jurusan,
		);
		
		if(view()->exists($view))
		{
            return view($view, $view_data)->render();
		}
    }

    public function query_tambah_siswa(Request $request)
    {
        $helper = new module_helper();
        $criteria = $request->all();
        $helper = new module_helper();

        $tambah = $helper->query_tambah_siswa($criteria);

        if ($tambah) 
        {
            return redirect('siswa')->with('message', "Berhasil Menambahkan Siswa.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Menambahkan Siswa!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }

    public function edit_siswa($id)
    {
        $helper = new module_helper();
		$id = base64_decode($id);
		
        $data = $helper->ambil_data($id);

        if (count($data) == 0) 
        {
            return redirect('siswa');
        }

        if (count($data)) 
        {
            $data['password_siswa'] = base64_decode($data['password_siswa']);
        }
        $list_jurusan = $helper->list_jurusan();
        $list_kelas = $helper->list_kelas();

		
        $view = 'Siswa::create';

		$view_data = array(
			'data'              => $data,
			'page'				=> "edit",
			'list_jurusan'	=> $list_jurusan,
			'list_kelas'		=> $list_kelas,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    public function query_edit_siswa(Request $request)
    {
        $criteria = $request->all();
        $helper = new module_helper();

        $edit = $helper->query_edit_siswa($criteria);

        if ($edit) 
        {
            return redirect('siswa')->with('message', "Siswa Berhasil Dirubah.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Merubah Siswa!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }

    public function hapus_siswa($id)
    {
        $helper = new module_helper();
		$id = base64_decode($id);

        $hapus = $helper->query_hapus_data($id);

        if ($hapus) 
        {
            return redirect('siswa')->with('message', "Siswa Berhasil Dihapus.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Menghapus Siswa!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }
}
