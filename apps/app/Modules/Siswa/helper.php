<?php
namespace App\Modules\Siswa;

class SiswaHelper
{
  var $model_siswa        = 'App\Modules\Siswa\Models\Siswa';
  var $model_jurusan        = 'App\Modules\Jurusan\Models\Jurusan';
	var $model_kelas        = 'App\Modules\Kelas\Models\Kelas';

  function daftar_data()
  {
    $result = array();

    $query = $this->model_siswa::leftjoin("jurusan", "jurusan.serial_id_jurusan", "=", "siswa.jurusan_id_siswa")
                                ->leftjoin("kelas", "kelas.serial_id_kelas", "=", "siswa.kelas_id_siswa")
                                ->get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function ambil_data($id)
  {
    $result = array();

    $query = $this->model_siswa::leftjoin("jurusan", "jurusan.serial_id_jurusan", "=", "siswa.jurusan_id_siswa")
                                ->leftjoin("kelas", "kelas.serial_id_kelas", "=", "siswa.kelas_id_siswa")
                                ->where("serial_id_siswa", $id)
                                ->first();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function list_kelas()
  {
    $result = array();

    $query = $this->model_kelas::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function list_jurusan()
  {
    $result = array();

    $query = $this->model_jurusan::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function query_tambah_siswa($data)
  {
    $result = false;
    $insert = array(
      'nama_siswa'               => (isset($data['nama_siswa']) && $data['nama_siswa'] !== "") ? $data['nama_siswa'] : "(nama siswa kosong)",
      'kelas_id_siswa'           => isset($data['kelas_id_siswa']) ? $data['kelas_id_siswa'] : 0,
      'jurusan_id_siswa'         => isset($data['jurusan_id_siswa']) ? $data['jurusan_id_siswa'] : 0,
      'sub_jurusan_siswa'         => isset($data['sub_jurusan_siswa']) ? $data['sub_jurusan_siswa'] : "",
      'nisn_siswa'         => isset($data['nisn_siswa']) ? $data['nisn_siswa'] : "",
      'username_siswa'         => isset($data['username_siswa']) ? $data['username_siswa'] : "",
      'password_siswa'         => isset($data['password_siswa']) ? base64_encode($data['password_siswa']) : "",
    );
    $query = $this->model_siswa::insert($insert);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function query_edit_siswa($data)
  {
    $result = false;
    $update = array(
      'nama_siswa'               => (isset($data['nama_siswa']) && $data['nama_siswa'] !== "") ? $data['nama_siswa'] : "(nama siswa kosong)",
      'kelas_id_siswa'           => isset($data['kelas_id_siswa']) ? $data['kelas_id_siswa'] : 0,
      'jurusan_id_siswa'         => isset($data['jurusan_id_siswa']) ? $data['jurusan_id_siswa'] : 0,
      'sub_jurusan_siswa'         => isset($data['sub_jurusan_siswa']) ? $data['sub_jurusan_siswa'] : "",
      'nisn_siswa'         => isset($data['nisn_siswa']) ? $data['nisn_siswa'] : "",
      'username_siswa'         => isset($data['username_siswa']) ? $data['username_siswa'] : "",
      'password_siswa'         => isset($data['password_siswa']) ? base64_encode($data['password_siswa']) : "",
    );
    $query = $this->model_siswa::where("serial_id_siswa", $data['serial_id_siswa'])->update($update);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function query_hapus_data($id)
  {
    $result = false;
    $query = $this->model_siswa::where("serial_id_siswa", $id)->delete();

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }
  
}