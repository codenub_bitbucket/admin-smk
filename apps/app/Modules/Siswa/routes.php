<?php

Route::group(['module' => 'Siswa', 'middleware' => ['web'], 'namespace' => 'App\Modules\Siswa\Controllers'], function() {

    Route::get('siswa', 'SiswaController@daftar_data');
    Route::get('siswa/tambah', 'SiswaController@tambah_siswa');
    Route::post('siswa/tambah/query', 'SiswaController@query_tambah_siswa');
    Route::get('siswa/edit/{id}', 'SiswaController@edit_siswa');
    Route::post('siswa/edit/query', 'SiswaController@query_edit_siswa');
    Route::get('siswa/hapus/{id}', 'SiswaController@hapus_siswa');

});
