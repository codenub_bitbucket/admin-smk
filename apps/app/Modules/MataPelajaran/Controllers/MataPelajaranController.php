<?php

namespace App\Modules\MataPelajaran\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\MataPelajaran\MataPelajaranHelper as module_helper;

class MataPelajaranController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
    }

    public function daftar_data()
    {
        $helper = new module_helper();
        $daftar_data = $helper->daftar_data();
        $daftar_kelas = $helper->daftar_kelas();
        $view = 'MataPelajaran::index';

		$view_data = array(
            'daftar_kelas'               => $daftar_kelas,
			'matapelajaran'              => $daftar_data,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    public function cari(Request $request)
    {
        $helper = new module_helper();
        $cari_data = $helper->cari_data($request->all());

		return $cari_data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah_data($kelas, Request $request)
    {
        $helper = new module_helper();

        $valid_kelas = $helper->kelasKode($kelas);

        if($valid_kelas == 0)
        {
            return redirect('mata-pelajaran');
        }
        $daftar_jurusan = $helper->daftar_jurusan();
        $view = 'MataPelajaran::create';

		$view_data = array(
            'page'                    => 'create',
            'kelas'                   => $kelas,
			'daftar_jurusan'          => $daftar_jurusan,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function query_tambah(Request $request)
    {
        $criteria = $request->all();
        $helper = new module_helper();

        $tambah = $helper->query_tambah_data($criteria);

        if ($tambah) 
        {
            return redirect('mata-pelajaran')->with('message', "Berhasil Menambahkan Mata Pelajaran.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Menambahkan Mata Pelajaran!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }

    public function edit($id)
    {
		$helper = new module_helper();
		$id = base64_decode($id);
		
        $data = $helper->ambil_data($id);
        if (count($data) == 0) 
        {
            return redirect('mata-pelajaran');
        }
        $daftar_jurusan = $helper->daftar_jurusan();
        $daftar_kelas = $helper->daftar_kelas();

		
        $view = 'MataPelajaran::create';

		$view_data = array(
			'data'              => $data,
			'page'				=> "edit",
			'daftar_jurusan'	=> $daftar_jurusan,
			'daftar_kelas'		=> $daftar_kelas,
		);
		
		if(view()->exists($view))
		{
				return view($view, $view_data)->render();
		}
    }

    public function query_edit(Request $request)
    {
        $criteria = $request->all();
        $helper = new module_helper();

        $edit = $helper->query_edit_data($criteria);

        if ($edit) 
        {
            return redirect('mata-pelajaran')->with('message', "Mata Pelajaran Berhasil Dirubah.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Merubah Mata Pelajaran!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }

    public function hapus_data($id)
    {
        $helper = new module_helper();
		$id = base64_decode($id);

        $hapus = $helper->query_hapus_data($id);

        if ($hapus) 
        {
            return redirect('mata-pelajaran')->with('message', "Mata Pelajaran Berhasil Dihapus.")
                                            ->with('class', 'success')
                                            ->with('type', 'success');
        }
        else
        {
            return redirect()->back()->with('message', "Gagal Menghapus Mata Pelajaran!")
                                    ->with('class', 'danger')
                                    ->with('type', 'failed');
        }
    }
}
