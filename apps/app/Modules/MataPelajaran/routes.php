<?php

Route::group(['module' => 'MataPelajaran', 'middleware' => ['web'], 'namespace' => 'App\Modules\MataPelajaran\Controllers'], function() {

    Route::get('mata-pelajaran', 'MataPelajaranController@daftar_data');
    Route::get('mata-pelajaran/tambah/{kelas}', 'MataPelajaranController@tambah_data');
    Route::post('mata-pelajaran/tambah/query', 'MataPelajaranController@query_tambah');
    Route::get('mata-pelajaran/edit/{id}', 'MataPelajaranController@edit');
    Route::post('mata-pelajaran/edit/query', 'MataPelajaranController@query_edit');
    Route::get('mata-pelajaran/hapus/{id}', 'MataPelajaranController@hapus_data');
    Route::get('mata-pelajaran/cari', 'MataPelajaranController@cari');

});
