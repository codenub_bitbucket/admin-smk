<?php
namespace App\Modules\MataPelajaran;

class MataPelajaranHelper
{

	var $model_matapelajaran        = 'App\Modules\MataPelajaran\Models\MataPelajaran';
	var $model_jurusan        = 'App\Modules\Jurusan\Models\Jurusan';
	var $model_kelas        = 'App\Modules\Kelas\Models\Kelas';
	var $model_soal        = 'App\Modules\Soal\Models\Soal';

  function daftar_data()
  {
    $result = array();

    $query = $this->model_matapelajaran::leftjoin("jurusan", "jurusan.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->leftjoin("kelas", "kelas.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function daftar_jurusan()
  {
    $result = array();

    $query = $this->model_jurusan::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function daftar_kelas()
  {
    $result = array();

    $query = $this->model_kelas::get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function query_tambah_data($data)
  {
    $result = false;
    $insert = array(
      'nama_mata_pelajaran'               => (isset($data['nama_mata_pelajaran']) && $data['nama_mata_pelajaran'] !== "") ? $data['nama_mata_pelajaran'] : "(nama pelajaran kosong)",
      'durasi_pengerjaan_mata_pelajaran'  => (isset($data['durasi_pengerjaan_mata_pelajaran']) && $data['durasi_pengerjaan_mata_pelajaran'] !== "") ? $data['durasi_pengerjaan_mata_pelajaran'] : "0",
      'kelas_id_mata_pelajaran'           => $this->kelasKode($data['kelas_id_mata_pelajaran']),
      'jurusan_id_mata_pelajaran'         => isset($data['jurusan_id_mata_pelajaran']) ? $data['jurusan_id_mata_pelajaran'] : 0,
      'tanggal_ujian_mata_pelajaran'         => isset($data['tanggal_ujian_mata_pelajaran']) ? date('Y/m/d', strtotime($data['tanggal_ujian_mata_pelajaran'])) : date("Y/m/d"),
    );
    $query = $this->model_matapelajaran::insert($insert);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function query_edit_data($data)
  {
    $result = false;
    $insert = array(
      'nama_mata_pelajaran'               => (isset($data['nama_mata_pelajaran']) && $data['nama_mata_pelajaran'] !== "") ? $data['nama_mata_pelajaran'] : "(nama pelajaran kosong)",
      'durasi_pengerjaan_mata_pelajaran'  => (isset($data['durasi_pengerjaan_mata_pelajaran']) && $data['durasi_pengerjaan_mata_pelajaran'] !== "") ? $data['durasi_pengerjaan_mata_pelajaran'] : "0",
      'kelas_id_mata_pelajaran'           => $this->kelasKode($data['kelas_id_mata_pelajaran']),
      'jurusan_id_mata_pelajaran'         => isset($data['jurusan_id_mata_pelajaran']) ? $data['jurusan_id_mata_pelajaran'] : 0,
      'tanggal_ujian_mata_pelajaran'         => isset($data['tanggal_ujian_mata_pelajaran']) ? date('Y/m/d', strtotime($data['tanggal_ujian_mata_pelajaran'])) : date("Y/m/d"),
    );
    $query = $this->model_matapelajaran::where("serial_id_mata_pelajaran", $data['serial_id_mata_pelajaran'])->update($insert);

    if ($query) 
    {
      $result = true;
    }

    return $result;
  }

  function query_hapus_data($id)
  {
    $result = false;
    $query = $this->model_matapelajaran::where("serial_id_mata_pelajaran", $id)->delete();
    $query2 = $this->model_soal::where("mata_pelajaran_id_soal", $id)->delete();

    if ($query && $query2) 
    {
      $result = true;
    }

    return $result;
  }

  function kelasKode($kode)
  {
    $result = 0;
    $query  = $this->model_kelas::where('kode_kelas', "=", $kode)
                                ->first();

    if (count($query) > 0) 
    {
      $query = json_decode(json_encode($query), true);
      
      $result = $query['serial_id_kelas'];
    }

    return $result;
  }

  function ambil_data($id)
  {
    $result = array();

    $query = $this->model_matapelajaran::leftjoin("jurusan", "jurusan.serial_id_jurusan", "=", "mata_pelajaran.jurusan_id_mata_pelajaran")
                                ->leftjoin("kelas", "kelas.serial_id_kelas", "=", "mata_pelajaran.kelas_id_mata_pelajaran")
                                ->where("serial_id_mata_pelajaran", $id)
                                ->first();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }

  function cari_data($criteria)
  {
    $result = array();

    $query = $this->model_matapelajaran::where('kelas_id_mata_pelajaran', $criteria['kelas_id'])
                                       ->where('jurusan_id_mata_pelajaran', $criteria['jurusan_id'])
                                       ->get();

    if (count($query)) 
    {
      $result = json_decode(json_encode($query), true);
    }

    return $result;
  }
}
