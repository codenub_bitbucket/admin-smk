@extends('layouts.app')

@section('title', 'Mata Pelajaran')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
<li class="breadcrumb-item active"><a href="#">Mata Pelajaran</a></li>
@endsection

@section('content')
<div class="">
  <div class="card">
    <div class="card-header">
      <h2 class="card-title">Daftar Mata Pelajaran</h2>
      <div class="text-right">
        <div class="dropdown">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Tambah
          </button>
          <div class="dropdown-menu">
            @foreach($daftar_kelas as $key => $value)
            <a class="dropdown-item" href="{{ URL('mata-pelajaran/tambah/'.$value['kode_kelas']) }}"> {{ $value['nomor_kelas'] }} ({{ $value['nama_kelas'] }})</a>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
              
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Jurusan</th>
          <th>Tanggal Ujian</th>
          <th>Durasi Pengerjaan</th>
          <th>###</th>
        </tr>
        </thead>
        <tbody>
          @foreach($matapelajaran as $key => $value)
          <tr>
            <td>{{ $value['nama_mata_pelajaran'] }}</td>
            <td>{{ $value['nomor_kelas'] }} ({{ $value['nama_kelas'] }})</td>
            <td>{{ $value['nama_jurusan'] }}</td>
            <td>{{ date("d-m-Y", strtotime($value['tanggal_ujian_mata_pelajaran'])) }}</td>
            <td>{{ $value['durasi_pengerjaan_mata_pelajaran'] }}</td>
            <td>
              <a class="btn btn-primary btn-sm" href="{{ URL('mata-pelajaran/edit/'.base64_encode($value['serial_id_mata_pelajaran'])) }}">Edit</a>
              <a class="btn btn-danger btn-sm text-white" onclick="konfirmasi_hapus('<?= base64_encode($value['serial_id_mata_pelajaran']) ?>')">Hapus</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
<script src="{{ URL('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL('assets/plugins/toastr/toastr.min.js') }}"></script>

  @if (Session::has('message'))
    <script>
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 5000
      });
      
      Toast.fire({
        icon: "{{ Session::get('class') }}",
        title: " {{ Session::get('message') }} "
      })
    </script>
  @endif

  <script>
    function konfirmasi_hapus(id) 
    {
      Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Mata Pelajaran dan Soal akan terhapus secara permanen!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya'
      }).then((result) => {
        if (result.value) 
        {
          window.location = "{{ URL('mata-pelajaran/hapus') }}" + "/" + id;
        }
      });
    }
  </script>

@endsection