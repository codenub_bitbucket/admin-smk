@extends('layouts.app')

@if($page == "edit")
  @section('title', 'Edit Mata Pelajaran')
@else
  @section('title', 'Tambah Mata Pelajaran')
@endif

@if($page == "edit")
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('mata-pelajaran') }}">Mata Pelajaran</a></li>
  <li class="breadcrumb-item active"><a href="#">Edit</a></li>
  @endsection
@else
  @section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ URL('beranda') }}">Beranda</a></li>
  <li class="breadcrumb-item"><a href="{{ URL('mata-pelajaran') }}">Mata Pelajaran</a></li>
  <li class="breadcrumb-item active"><a href="#">Tambah</a></li>
  @endsection
@endif

@section('content')
<!-- Content Header (Page header) -->
<section class="content">
  <div class="container-fluid">
    <div class="card card-info">
      <div class="card-header">
        @if($page == "edit")
        <h3 class="card-title">Edit Mata Pelajaran</h3>
        @else
        <h3 class="card-title">Formulir Penambahan Mata Pelajaran Kelas {{ strtoupper($kelas) }}</h3>
        @endif
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form class="form-horizontal" method="POST" action="{{ ($page == 'edit') ? URL('mata-pelajaran/edit/query') : URL('mata-pelajaran/tambah/query') }}">
       {{ csrf_field() }}
        <div class="card-body">
          @if($page == "edit")
          <input type="hidden" name="serial_id_mata_pelajaran" value="{{$data['serial_id_mata_pelajaran']}}">
          <div class="form-group row">
            <label for="kelas_id_mata_pelajaran" class="col-sm-2 col-form-label">Kelas</label>
            <div class="col-sm-10">
              <select class="form-control" name="kelas_id_mata_pelajaran">
                @foreach($daftar_kelas as $key => $row)
                <option 
                {{ ($data['kelas_id_mata_pelajaran'] == $row['serial_id_kelas']) ? 'selected' : '' }}
                value="{{ $row['kode_kelas'] }}">{{ $row['nomor_kelas']}}</option>
                @endforeach
              </select>
            </div>
          </div>
          @else
          <input type="hidden" name="kelas_id_mata_pelajaran" value="{{$kelas}}">
          @endif
          <div class="form-group row">
            <label for="mata-pelajaran-name" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              @if($page == "edit")
              <input type="text" class="form-control" name="nama_mata_pelajaran" id="mata-pelajaran-name" placeholder="Nama Pelajaran" value="{{ $data['nama_mata_pelajaran'] }}" autocomplete="off">
              @else
              <input type="text" class="form-control" name="nama_mata_pelajaran" id="mata-pelajaran-name" placeholder="Nama Pelajaran">
              @endif
            </div>
          </div>
          <div class="form-group row">
            <label for="durasi-pengerjaan" class="col-sm-2 col-form-label">Durasi Pengerjaan Soal</label>
            <div class="col-sm-10">
              @if($page == "edit")
              <input type="number" name="durasi_pengerjaan_mata_pelajaran" class="form-control" id="durasi-pengerjaan" placeholder="Durasi Pengerjaan (menit)" value="{{ $data['durasi_pengerjaan_mata_pelajaran'] }}" autocomplete="off">
              @else
              <input type="number" name="durasi_pengerjaan_mata_pelajaran" class="form-control" id="durasi-pengerjaan" placeholder="Durasi Pengerjaan (menit)">
              @endif
            </div>
          </div>
          <div class="form-group row">
            <label for="durasi-pengerjaan" class="col-sm-2 col-form-label">Jurusan</label>
            <div class="col-sm-10">
              @if($page == "edit")
              <select class="form-control" name="jurusan_id_mata_pelajaran">
                @foreach($daftar_jurusan as $key => $row)
                <option 
                {{ ($data['jurusan_id_mata_pelajaran'] == $row['serial_id_jurusan']) ? 'selected' : '' }}
                value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
                @endforeach
              </select>
              @else
              <select class="form-control" name="jurusan_id_mata_pelajaran">
                @foreach($daftar_jurusan as $key => $row)
                <option value="{{ $row['serial_id_jurusan'] }}">{{ $row['nama_jurusan'] }}</option>
                @endforeach
              </select>
              @endif
            </div>
          </div>
          <div class="form-group row">
            <label for="durasi-pengerjaan" class="col-sm-2 col-form-label">Tanggal Pengerjaan Soal</label>
            <div class="col-sm-10">
              @if($page == 'edit')
              <div class="input-group date" id="reservationdate" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" id="tanggal_ujian_mata_pelajaran" name="tanggal_ujian_mata_pelajaran" value="{{ date('M d, Y', strtotime($data['tanggal_ujian_mata_pelajaran'])) }}" autocomplete="off"/>
                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
              </div>
              @else
              <div class="input-group date" id="reservationdate" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" id="tanggal_ujian_mata_pelajaran" name="tanggal_ujian_mata_pelajaran"/>
                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <a class="btn btn-danger text-white" href="{{ URL('mata-pelajaran') }}">Cancel</a>
          @if($page == 'edit')
          <button type="submit" class="btn btn-info float-right">Simpan Perubahan</button>
          @else
          <button type="submit" class="btn btn-info float-right">Tambah</button>
          @endif
        </div>
        <!-- /.card-footer -->
      </form>
    </div>
  </div>
</section>

<script>

if ("{{ $page == 'create' }}") 
{
    console.log('gak hasrus');
  $('#tanggal_ujian_mata_pelajaran').val(moment().local().format("ll"));  
}
$('#reservationdate').datetimepicker({
    format: 'll'
});

</script>
@endsection