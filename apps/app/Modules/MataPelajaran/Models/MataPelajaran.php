<?php

namespace App\Modules\MataPelajaran\Models;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model {

    protected $table 		= "mata_pelajaran";
	protected $primaryKey 	= "serial_id_mata_pelajaran";
	protected $guarded = array('serial_id_mata_pelajaran');
	public $timestamps = false;

}
