<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_ujian', function (Blueprint $table) {
            $table->increments('serial_id_hasil_ujian');
            $table->bigInteger("siswa_id_hasil_ujian");
            $table->bigInteger("kelas_id_hasil_ujian");
            $table->bigInteger("jurusan_id_hasil_ujian");
            $table->bigInteger("mata_pelajaran_id_hasil_ujian");
            $table->text("jawaban_hasil_ujian");
            $table->text("final_hasil_ujian");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hasil_ujian');
    }
}
