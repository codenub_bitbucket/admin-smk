<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal', function (Blueprint $table) {
            $table->increments('serial_id_soal');
            $table->string('nomor_soal');
            $table->longText('deskripsi_soal');
            $table->integer('mata_pelajaran_id_soal');
            $table->integer('kelas_id_soal');
            $table->integer('jurusan_id_soal');
            $table->text('jawaban_soal');
            $table->text('kunci_jawaban_soal');
            $table->text('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soal');
    }
}
