<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->increments('serial_id_siswa');
            $table->string('nama_siswa');
            $table->string('username_siswa');
            $table->string('password_siswa');
            $table->string('nisn_siswa');
            $table->bigInteger('kelas_id_siswa');
            $table->bigInteger('jurusan_id_siswa');
            $table->string('sub_jurusan_siswa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswa');
    }
}
