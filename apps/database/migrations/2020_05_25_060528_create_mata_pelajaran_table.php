<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMataPelajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_pelajaran', function (Blueprint $table) {
            $table->increments('serial_id_mata_pelajaran');
            $table->string('nama_mata_pelajaran')->nullable();
            $table->string('durasi_pengerjaan_mata_pelajaran')->nullable();
            $table->bigInteger('kelas_id_mata_pelajaran')->nullable();
            $table->bigInteger('jurusan_id_mata_pelajaran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mata_pelajaran');
    }
}
