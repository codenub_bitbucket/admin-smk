<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class jurusan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jurusan')->truncate();
        
        DB::table('jurusan')->insert([
            [
                'nama_jurusan' => 'TKJ',
            ],
            [
                'nama_jurusan' => 'STM',
            ],
            [
                'nama_jurusan' => 'AKUNTANSI',
            ]
                    
      ]);
    }
}
