<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kelas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas')->truncate();
    		
        DB::table('kelas')->insert([
        		[
                    'nama_kelas' => 'Sepuluh',
                    'nomor_kelas' => 'X',
                    'kode_kelas' => 'x',
                ],
                [
                    'nama_kelas' => 'Sebelas',
                    'nomor_kelas' => 'XI',
                    'kode_kelas' => 'xi',
                ],
                [
                    'nama_kelas' => 'Dua Belas',
                    'nomor_kelas' => 'XII',
                    'kode_kelas' => 'xii',
                ]
						
      	]);
    }
}
