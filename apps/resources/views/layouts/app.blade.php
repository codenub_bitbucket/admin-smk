<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title') - Admin Panel SMK Kartikatama</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="image/png" href="{{ URL('assets/image/logo.png') }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL('assets/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ URL('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">  
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ URL('assets/plugins/toastr/toastr.min.css') }}">

  <link rel="stylesheet" href="{{ URL('assets/plugins/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ URL('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ URL('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ URL('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('assets/dist/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ URL('assets/dist/js/demo.js') }}"></script>

    <!-- PAGE {{ URL('assets/plugins/fontawesome-free/css/all.min.css') }}PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ URL('assets/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ URL('assets/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ URL('assets/plugins/chart.js/Chart.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ URL('assets/dist/js/pages/dashboard2.js') }}"></script>

    <!-- DataTables -->
    <script src="{{ URL('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ URL('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ URL('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <meta name="_token" content="{{ csrf_token() }}">
    <script>
      $(function () {
        $("#example1").DataTable({
          "responsive": true,
          "autoWidth": false,
        });
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });

      });
    </script>
</head>
  <body class="hold-transition layout-top-nav layout-footer-fixed">
    <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a href="{{ URL('beranda') }}" class="navbar-brand">
            <img src="{{ URL('assets/image/logo.png') }}" alt="SMK Kartikatama" class="brand-image img-circle elevation-2"
                style="opacity: .8">
            <span class="brand-text font-weight-light">SMK Kartikatama</span>
          </a>
          
          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{ URL('beranda') }}" class="nav-link">Beranda</a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('mata-pelajaran') }}" class="nav-link">Mata Pelajaran</a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('soal') }}" class="nav-link">Soal</a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('siswa') }}" class="nav-link">Siswa</a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('hasil-ujian') }}" class="nav-link">Hasil Ujian</a>
              </li>
            </ul>
          </div>

          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown">
              <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">{{ Session::get('users_name') }}</a>
              <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <!-- <li><a href="#" class="dropdown-item">Some action </a></li>
                <li><a href="#" class="dropdown-item">Some other action</a></li>

                <li class="dropdown-divider"></li> -->

                <li><a href="{{ URL('/logout') }}" class="dropdown-item">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark"> @yield('title') | <small>Admin Panel SMK</small></h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  @yield('breadcrumb')
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="container">
            @yield('content')
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-block">
          SMK KARTIKATAMA 1 METRO
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2019-2020 | Rendi Febrian</strong> All rights reserved.
      </footer>
    </div>
  </body>
</html>
